#pragma once

#ifndef UPCXXA_COORDINATE_TYPE
# define UPCXXA_COORDINATE_TYPE int
#endif

namespace upcxx {
  typedef UPCXXA_COORDINATE_TYPE cint_t;
}

#ifndef UPCXXA_MAX_SPECIALIZED_ARITY
# define UPCXXA_MAX_SPECIALIZED_ARITY 9
#endif

#ifdef UPCXXA_MYPROC
# define UPCXXA_DMYPROC UPCXXA_MYPROC
#else
# define UPCXXA_DMYPROC 0
#endif

#ifdef __COUNTER__
# define UPCXXA_UNIQUIFY(prefix) UPCXXA_CONCAT_(prefix, __COUNTER__)
#else
# define UPCXXA_UNIQUIFY(prefix) UPCXXA_CONCAT_(prefix, __LINE__)
#endif
#define UPCXXA_UNIQUIFYN(prefix, name)          \
  UPCXXA_UNIQUIFY(prefix ## name ## _)

#define UPCXXA_CONCAT_(a, b)                    \
  UPCXXA_CONCAT__(a, b)
#define UPCXXA_CONCAT__(a, b)                   \
  a ## b
#define UPCXXA__CONCAT UPCXXA_CONCAT_
#define UPCXXA__IDENTITY(x) (x)

#define UPCXXA_ZERO_POINT point<N>::all(0)
#define UPCXXA_ONES_POINT point<N>::all(1)
#define UPCXXA_NEGONE_POINT point<N>::all(-1)

// Enable checking for domain errors by default
#ifndef UPCXXA_DOMAINS_USERCHECK
# define UPCXXA_DOMAINS_USERCHECK 1
#endif

/**Following sections define error-checking macros
 * UPCXXA_CHECK - check for a user-generated error (ie, validate user
 *         input), dump error to System.out on failure. Enabled by
 *         #define UPCXXA_DOMAINS_USERCHECK.
 * UPCXXA_CHECKF - as UPCXXA_CHECK, but also perform a
 *         programmer-defined failure action. Enabled by #define
 *         UPCXXA_DOMAINS_USERCHECK.
 */

#if UPCXXA_DOMAINS_USERCHECK
  //define macro for user-code error checking, without and with
  //conditional failure execution
# define UPCXXA_CHECK(assertion,errmsg)                         \
  do {                                                          \
    if (!(assertion))                                           \
      std::cout << "Runtime: (Error) " << errmsg << std::endl;  \
  } while(0)
# define UPCXXA_CHECKF(assertion,errmsg,failureaction)          \
  do {                                                          \
    if (!(assertion)) {                                         \
      std::cout << "Runtime: (Error) " << errmsg << std::endl;  \
      failureaction                                             \
    }                                                           \
  } while(0)
#else
# define UPCXXA_CHECK(a,b)
# define UPCXXA_CHECKF(a,b,c)
#endif

#define UPCXXA_CHECK_PERMUTE(p, retval)                                 \
  UPCXXA_CHECKF(domain_utils::checkPermute(p),                          \
                "point<N> does not result in a valid permute operation", \
                return retval;);

#if defined(BOUNDS_CHECKING) && !defined(UPCXXA_BOUNDS_CHECKING)
# define UPCXXA_BOUNDS_CHECKING BOUNDS_CHECKING
#endif
// Disable bounds checking by default
#ifndef UPCXXA_BOUNDS_CHECKING
# define UPCXXA_BOUNDS_CHECKING 0
#endif

// Enable checking for non-bounds array errors by default
#ifndef UPCXXA_ARRAYS_USERCHECK
# define UPCXXA_ARRAYS_USERCHECK 1
#endif

#ifdef UPCXXA_DEFAULT_CMAJOR
# define UPCXXA_DEFAULT_STRIDEDNESS_ column
# define UPCXXA_DEFAULT_LAYOUT(S, N)            \
  (!UPCXXA_FLAG_EQUAL(S, row) || N == 1)
#else
# define UPCXXA_DEFAULT_STRIDEDNESS_ row
# define UPCXXA_DEFAULT_LAYOUT(S, N)            \
  (UPCXXA_FLAG_EQUAL(S, column) && N > 1)
#endif

#ifndef UPCXXA_DEFAULT_STRIDEDNESS
# define UPCXXA_DEFAULT_STRIDEDNESS UPCXXA_DEFAULT_STRIDEDNESS_
#endif

#if !defined(NDEBUG) && !defined(UPCXXA_DEBUG_ARRAYS)
# define UPCXXA_UNDEF_NDEBUG
# define NDEBUG
#endif

#ifdef UPCXXA_HAVE_GASNET_TOOLS
# define UPCXXA_PREDICT_TRUE GASNETT_PREDICT_TRUE
# define UPCXXA_PREDICT_FALSE GASNETT_PREDICT_FALSE
# define UPCXXA_if_pf if_pf
# define UPCXXA_if_pt if_pt
#else
# define UPCXXA_PREDICT_TRUE UPCXXA__IDENTITY
# define UPCXXA_PREDICT_FALSE UPCXXA__IDENTITY
# define UPCXXA_if_pf if
# define UPCXXA_if_pt if
#endif

/* optimization: trading multiplies for branches in the case where
   we have strong reason to believe one of the operands could be 1
   empirically shown to help performance on Power3 and x86
*/
#define UPCXXA_PFAST_MULT(a,b)                          \
  (UPCXXA_PREDICT_TRUE((b) == 1) ? (a) : (a)*(b) )
#define UPCXXA_PFAST_DIVIDE(a,b)                        \
  (UPCXXA_PREDICT_TRUE((b) == 1) ? (a) : (a)/(b) )

#ifdef USE_THREAD_LOCAL_TMPS
# define UPCXXA_THREAD_LOCAL(type, name) static thread_local type name
# define UPCXXA_THREAD_LOCAL_BUF UPCXXA_THREAD_LOCAL
# define UPCXXA_THREAD_LOCAL_BUF_FREE(name, free)
# define UPCXXA_THREAD_LOCAL_BUF_SIZE(static_sz, stack_sz) static_sz
#else
# define UPCXXA_THREAD_LOCAL(type, name) type name
# define UPCXXA_THREAD_LOCAL_BUF(type, name) type name = (type) NULL
# define UPCXXA_THREAD_LOCAL_BUF_FREE(name, free) free(name)
# define UPCXXA_THREAD_LOCAL_BUF_SIZE(static_sz, stack_sz) stack_sz
#endif
