# This script generates specializations for array wrapper methods. The
# maximum arity of the specializations to be generated should be
# provided as a command line argument:
#   python gen_array_wrapper_specializations.py <max_arity>
# The default maximum arity is 9.

from sys import argv

max_arity = 9

op_paren_skeleton = '''   {4} ref_type operator()({1}){4} {{
#ifdef UPCXXA_USE_CXX11
      static_assert(N == {0},
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              {2})];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT({3}));
#endif
    }}
'''

def gen_list(arity, var, indent, ptype=None, break_after=1):
    if arity == 0:
        return ''
    prefix = '' if ptype == None else (ptype + ' ')
    lst = prefix + var + str(0)
    for i in range(1, arity):
        if i % break_after == 0:
            lst += ',\n' + (' ' * indent)
        else:
            lst += ', '
        lst += prefix + var + str(i)
    return lst

def gen_op_parens():
    for i in range(1, max_arity+1):
        params = gen_list(i, 'i', 24, 'cint_t', 4)
        params2 = gen_list(i, 'i', 30, 'cint_t', 3)
        args = gen_list(i, 'i', 46, None, 6)
        args2 = gen_list(i, 'i', 27, None, 10)
        print(op_paren_skeleton.format(i, params, args, args2, ''))
        print(op_paren_skeleton.format(i, params2, args, args2, ' const'))

if __name__ == '__main__':
    if len(argv) > 1:
        max_arity = int(argv[1])
    gen_op_parens()
