#pragma once

#ifdef UPCXXA_USE_CXX11
# include <type_traits>
#endif

namespace domain_utils {

  template<class T> inline T gcd(T a, T b) {
    return (b == 0) ? a : gcd(b, a % b);
  }

  template<class T> inline T lcm(T a, T b) {
    return ((a == 0) || (b == 0)) ? 0 : (a * (b / gcd(a, b)));
  }

  template<class T> inline T max(T a, T b) {
    return (a < b) ? b : a;
  }

  template<class T> inline T min(T a, T b) {
    return (a < b) ? a : b;
  }

  template<class T> inline T abs(T a) {
    return (a < 0) ? -a : a;
  }

  template<class T> inline T align_down(T value, T alignment) {
    return value - (value % alignment);
  }

  template<class T> inline T align_up(T value, T alignment) {
    return align_down(value + alignment - 1, alignment);
  }

  inline int direction_value(int dir) {
    // return (dir < 0) ? -dir : dir;
    return (dir < 0) ? -(dir+1) : dir-1;
  }

  inline bool direction_backward(int dir) {
    return (dir < 0);
  }

  inline int sign(int dir) {
    return dir < 0 ? -1 : 1;
  }

  template<class T> inline T div_round_to_minus_inf(T a, T b) {
    /*if ((a > 0) == (b > 0)) return a / b;*/
    if ((a^b) >= 0) return a / b;
    else if (b > 0) return (a - (b - 1)) / b;
    else return (a - (b + 1)) / b;
  }

  template<int N> bool checkPermute(const upcxx::point<N> &p) {
    int bitFlags = 0;
    int err = 0;
    int i;
    for (i = 0; (i < N) && (!err); i++) {
      int p_i = p.x[i];
      if ((p_i <= 0) || (p_i > N)) {
        err = 1;
      } else {
        int mask = ((int) 1) << (p_i-1);
        if (bitFlags & mask) {
          err = 1;
        } else {
          bitFlags = bitFlags | mask;
        }
      }
    }
    return ((bitFlags == ((((int) 1) << N) - ((int) 1))) && (!err));
  }

  // Used to cut off template recursion at 1
  template<int N> struct n_minus_1 {
    enum { arity = N-1 };
  };

  template<> struct n_minus_1<1> {
    enum { arity = 1 };
  };

  // Used to distinguish constructor called by RECTDOMAIN macro
  template<int N> struct n_equal_1 {
    enum { arity = 1 };
  };

  template<> struct n_equal_1<1> {
    enum { arity = 0 };
  };

  // Used to avoid compiler warnings in arity-specialized code
  template<int N, int M> struct static_min {
    enum { arity = (N > M ? M : N) };
  };

  template<class T, T N, T M> struct static_max {
    enum { arity = (N < M ? M : N) };
  };

  // Standard enable_if, in case we are using C++03
  template<bool B, class T = void> struct enable_if {};

  template<class T> struct enable_if<true, T> {
    typedef T type;
  };

  // Used with enable_if as an argument type that won't be
  // accidentally used by the user
  struct internal_type {};

  // Standard is_same
  template<class T, class U> struct is_same {
    enum { value = 0 };
  };

  template<class T> struct is_same<T, T> {
    enum { value = 1 };
  };

  // Used to restrict specializations to the right arity
  template<class T, int N, int M> struct int_and_eq {
    enum { value = 0 };
  };

  template<class T, int N> struct int_and_eq<T, N, N> {
#if defined(UPCXXA_USE_CXX11) && defined(UPCXXA_CHECK_INT_CONV)
    // check that T is convertible to upcxx::cint_t
    enum { value = std::is_convertible<T, upcxx::cint_t>::value };
#else
    // can't check convertibility; code will fail at some later point
    // if T is not convertible to upcxx::cint_t
    enum { value = 1 };
#endif
  };
}
