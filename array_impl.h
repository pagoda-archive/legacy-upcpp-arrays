/* array_impl.h */

#if !UPCXXA_GLOBAL_ARRAY
namespace domain_utils {
  template<int N> struct n_minus_1;
  template<class T, T N, T M> struct static_max;
  template<bool B, class T> struct enable_if;
}

# define UPCXXA_ARITY_N_MINUS_1 domain_utils::n_minus_1<N>::arity
#endif

namespace upcxx {

#if UPCXXA_GLOBAL_ARRAY
template<class T, int N> class UPCXXA_LOCAL_ARRAY_T;
#else
template<class T, int N> class UPCXXA_GLOBAL_ARRAY_T;
#endif
template<int N> struct index_computer;
template<class T, int N, class F1, class F2> class ndarray;
template<class T, int N, class G, class S>
struct index_slicer;

template<class T, int N> struct UPCXXA_COPY_DESCRIPTOR;
template<class T, int N>
T *UPCXXA_PACK(void *copy_desc_ptr,
               size_t *array_data_size,
               void *allocated);
template<class T, int N>
void UPCXXA_UNPACK(UPCXXA_COPY_DESCRIPTOR<T, N> *copy_desc_ptr,
                   T *buf);

template<class T, int N> class UPCXXA_ARRAY_T {
  /* Methods defined in array.cc */
 public:
  UPCXXA_ARRAY_T(const rectdomain<N> &R,
                 void *(*allocator)(size_t) = UPCXXA_ARRAY_MALLOC,
                 UPCXXA_PTR_TO_T ext_mem = (UPCXXA_PTR_TO_T)(T *)NULL,
                 point<N> padding = point<N>::all(0));

  UPCXXA_ARRAY_T() {
    A = ancestor = (UPCXXA_PTR_TO_T) (T *) NULL;
  }

  UPCXXA_ARRAY_T(void *) { // allow conversion from NULL
    A = ancestor = (UPCXXA_PTR_TO_T) (T *) NULL;
  }

#if UPCXXA_GLOBAL_ARRAY
  /* Allow implicit local->global conversion */
 UPCXXA_ARRAY_T(const UPCXXA_LOCAL_ARRAY_T<T, N> &arr) {
    UPCXXA_GLOBALIZE(A, arr.A);
    UPCXXA_GLOBALIZE(ancestor, arr.ancestor);
    _domain = arr._domain;
    memcpy(base, arr.base, sizeof(base));
    memcpy(stride, arr.stride, sizeof(stride));
    memcpy(side_factors, arr.side_factors, sizeof(side_factors));
# if UPCXXA_EXPLICITLY_STORE_CREATOR
    _creator = arr._creator;
# endif
  }
#else
  /* Only allow explicit global->local conversion */
  explicit UPCXXA_ARRAY_T(const UPCXXA_GLOBAL_ARRAY_T<T, N> &arr) {
# if UPCXXA_ARRAYS_USERCHECK
    if (!arr.is_null() && !arr.is_local()) {
      std::cerr << "failed to convert global array created by "
                << arr.creator() << " to local array on thread "
                << UPCXXA_MYPROC << std::endl;
      abort();
    }
# endif
    A = UPCXXA_TO_LOCAL(arr.A);
    ancestor = UPCXXA_TO_LOCAL(arr.ancestor);
    _domain = arr._domain;
    memcpy(base, arr.base, sizeof(base));
    memcpy(stride, arr.stride, sizeof(stride));
    memcpy(side_factors, arr.side_factors, sizeof(side_factors));
# if UPCXXA_EXPLICITLY_STORE_CREATOR
    _creator = arr._creator;
# endif
  }
#endif /* UPCXXA_GLOBAL_ARRAY */

  UPCXXA_ARRAY_T<T, UPCXXA_ARITY_N_MINUS_1> slice(int k,
                                                  cint_t j) const;
  void exchange(T w) const;
  void vbroadcast(int s) const;
  void set(T w) const;
  void copy(const UPCXXA_ARRAY_T &y,
            const upcxx::domain<N> &dom) const;
  void copy(const UPCXXA_ARRAY_T &y,
            const rectdomain<N> &rdom) const;
  void copy(const UPCXXA_ARRAY_T &y,
            const UPCXXA_ARRAY_T<point<N>, 1> &ptArray) const;
  void scatter(const UPCXXA_ARRAY_T<T, 1> &packedArray,
               const UPCXXA_ARRAY_T<point<N>, 1> &ptArray) const;
  void gather(const UPCXXA_ARRAY_T<T, 1> &packedArray,
              const UPCXXA_ARRAY_T<point<N>, 1> &ptArray) const;
  UPCXXA_ARRAY_T translate(const point<N> &p) const;
  UPCXXA_ARRAY_T permute(const point<N> &p) const;
  UPCXXA_ARRAY_T inject(const point<N> &p) const;
  UPCXXA_ARRAY_T project(const point<N> &p) const;
  bool is_contiguous() const;

 private:
  UPCXXA_PTR_TO_T get_array_data_ptr() const;
  UPCXXA_PTR_TO_T get_array_data_ptr_with_domain(const rectdomain<N> &R) const;
  void copy_inner(const UPCXXA_ARRAY_T &y,
                  UPCXXA_EVENT_T e) const;
  bool is_contiguous_over(const rectdomain<N> &R) const;
  bool internal_is_contiguous(const rectdomain<N> *pR,
                              size_t *pnumelements,
                              UPCXXA_PTR_TO_T *pRdataptr) const;
  UPCXXA_ARRAY_T make_local_and_contiguous() const;
  int may_overlap(const UPCXXA_ARRAY_T &y,
                  const rectdomain<N> &R) const;
  void bounds_check(const char *where, const point<N> &p) const;
  size_t compute_index(const char *where, const point<N> &p) const {
    UPCXXA_ASSERT_EXISTS((*this), where);
    return index_computer<N>::compute(base, stride, side_factors,
#if UPCXXA_BOUNDS_CHECKING
                                      _domain,
#endif
                                      where, p.x);
  }
  UPCXXA_ARRAY_T<T, UPCXXA_ARITY_N_MINUS_1> slice_internal(int k,
                                                           cint_t j) const;
 public:
  /* Delete this array. This array must be local. The user must ensure
   * that no views of the the original array are still in use. */
  void destroy(void (*deallocator)(void *) = UPCXXA_ARRAY_FREE) {
#if UPCXXA_ARRAYS_USERCHECK && UPCXXA_GLOBAL_ARRAY
    if (!is_null() && !is_local()) {
      std::cerr << "failed to destroy global array created by "
                << creator() << " on thread " << UPCXXA_MYPROC
                << std::endl;
      abort();
    }
#endif
    if (is_null() || /* no memory allocated */
        UPCXXA_LOCAL_PART(ancestor) == NULL) /* external memory provided */
      return;
    if (deallocator == std::free) { /* new[] used to allocate */
      delete[] UPCXXA_LOCAL_PART(ancestor);
      return;
    }
#ifdef UPCXXA_USE_CXX11_TRIVIALLY_DESTRUCTIBLE
    if (std::is_trivially_destructible<T>::value) {
      deallocator(UPCXXA_LOCAL_PART(ancestor));
      return;
    }
#endif
    size_t *mem = (size_t *)(void *) UPCXXA_LOCAL_PART(ancestor);
    T *buf = (T *)(void *) (mem + 1);
    size_t count = *mem;
    for (size_t i = 0; i < count; i++) {
      buf[i].~T();
    }
    deallocator(mem);
  }

  rectdomain<N> domain() const {
    UPCXXA_ASSERT_EXISTS((*this), "(in domain method)");
    return _domain;
  }

  void copy(const UPCXXA_ARRAY_T &y) const {
    copy_inner(y, UPCXXA_EVENT_NONE);
  }

  void async_copy(const UPCXXA_ARRAY_T &y,
                  UPCXXA_EVENT_T e = UPCXXA_DEFAULT_EVENT) const {
    copy_inner(y, e);
  }

#if !UPCXXA_GLOBAL_ARRAY
  /* Enable local-global operations not covered by implicit
   * conversions */
  void copy(const UPCXXA_GLOBAL_ARRAY_T<T, N> &y) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.copy(y);
  }

  void async_copy(const UPCXXA_GLOBAL_ARRAY_T<T, N> &y,
                  UPCXXA_EVENT_T e = UPCXXA_DEFAULT_EVENT) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.async_copy(y, e);
  }

  void copy(const UPCXXA_GLOBAL_ARRAY_T<T, N> &y,
            const upcxx::domain<N> &dom) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.copy(y, dom);
  }

  void copy(const UPCXXA_GLOBAL_ARRAY_T<T, N> &y,
            const rectdomain<N> &rdom) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.copy(y, rdom);
  }

  void copy(const UPCXXA_GLOBAL_ARRAY_T<T, N> &y,
            const UPCXXA_GLOBAL_ARRAY_T<point<N>, 1> &ptArray) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.copy(y, ptArray);
  }

  void scatter(const UPCXXA_GLOBAL_ARRAY_T<T, 1> &packedArray,
               const UPCXXA_GLOBAL_ARRAY_T<point<N>, 1> &ptArray) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.scatter(packedArray, ptArray);
  }

  void gather(const UPCXXA_GLOBAL_ARRAY_T<T, 1> &packedArray,
              const UPCXXA_GLOBAL_ARRAY_T<point<N>, 1> &ptArray) const {
    UPCXXA_GLOBAL_ARRAY_T<T, N> x(*this);
    x.gather(packedArray, ptArray);
  }
#endif /* !UPCXXA_GLOBAL_ARRAY */

  UPCXXA_ARRAY_T constrict(const rectdomain<N> &R) const {
    UPCXXA_ASSERT_EXISTS((*this), "(in constrict method)");
    UPCXXA_ARRAY_T n(*this);
    n._domain = _domain * R;
    return n;
  }

  UPCXXA_ARRAY_T shrink(cint_t k) const {
    UPCXXA_ASSERT_EXISTS((*this), "(in shrink method)");
    UPCXXA_ARRAY_T n(*this);
    n._domain = _domain.shrink(k); /* always safe to omit intersection */
    return n;
  }

  UPCXXA_ARRAY_T shrink(cint_t k, int dir) const {
    UPCXXA_ASSERT_EXISTS((*this), "(in shrink method)");
    UPCXXA_ARRAY_T n(*this);
    n._domain = _domain.shrink(k, dir); /* always safe to omit intersection */
    return n;
  }

  UPCXXA_ARRAY_T border(cint_t k, int dir, cint_t shift) const {
    UPCXXA_ASSERT_EXISTS((*this), "(in border method)");
    UPCXXA_ARRAY_T n(*this);
    /* intersection required in many cases for correctness */
    n._domain = _domain * _domain.border(k, dir, shift);
    return n;
  }

  UPCXXA_ARRAY_T border(cint_t k, int dir) const {
    return border(k, dir, 1);
  }

  UPCXXA_ARRAY_T border(int dir) const {
    return border(1, dir, 1);
  }

  size_t size() const {
    UPCXXA_ASSERT_EXISTS((*this), "(in size method)");
    return _domain.size();
  }

  bool is_empty() const {
    UPCXXA_ASSERT_EXISTS((*this), "(in is_empty method)");
    return _domain.is_empty();
  }

  int creator() const {
    UPCXXA_ASSERT_EXISTS((*this), "(in creator method)");
    /* creator returns "the lowest numbered rank whose demesne
       contains the object pointed to" */
#if UPCXXA_EXPLICITLY_STORE_CREATOR
    int boxid = 0, bproc = 0;
    UPCXXA_PROC_TO_BOXID_PROC(_creator, boxid, bproc);
    return (int) UPCXXA_BOX_TO_FIRST_PROC(UPCXXA_BOXID_TO_BOX(boxid));
#else
    return (int) UPCXXA_BOX_TO_FIRST_PROC(UPCXXA_BOX_PART(A));
#endif
  }

  UPCXXA_BOX_T where() const {
    return UPCXXA_BOX_PART(A);
  }

  bool is_local() const {
    UPCXXA_ASSERT_EXISTS((*this), "(in is_local method)");
#if !UPCXXA_GLOBAL_ARRAY
    return 1;
#else
    return UPCXXA_IS_DIRECTLY_ADDRESSABLE(A);
#endif
  }

  UPCXXA_PTR_TO_T storage_ptr() {
#if UPCXXA_ARRAYS_USERCHECK
    if (!is_null() && !is_contiguous()) {
      std::cerr << "retrieval of storage pointer only allowed "
                << "on contiguous arrays" << std::endl;
      abort();
    }
#endif
    return A;
  }

  const UPCXXA_PTR_TO_T storage_ptr() const {
#if UPCXXA_ARRAYS_USERCHECK
    if (!is_null() && !is_contiguous()) {
      std::cerr << "retrieval of storage pointer only allowed "
                << "on contiguous arrays" << std::endl;
      abort();
    }
#endif
    return A;
  }

  bool is_null() const {
    return UPCXXA_LOCAL_PART(A) == NULL;
  }

  bool operator==(const UPCXXA_ARRAY_T &y) const;

  bool operator!=(const UPCXXA_ARRAY_T &y) const {
    return !operator==(y);
  }

#if UPCXXA_GLOBAL_ARRAY
  friend class UPCXXA_LOCAL_ARRAY_T<T, N>;
#else
  friend class UPCXXA_GLOBAL_ARRAY_T<T, N>;
  /* Implement UPC++ local array interface */
  typedef domain_utils::enable_if<true, T> local_elem_type;
  /* Implement UPC++ globalization interface */
  typedef domain_utils::enable_if<true,
                                  UPCXXA_GLOBAL_ARRAY_T<T, N> > global_type;
#endif
  template<class T2, int N2> friend class UPCXXA_ARRAY_T;
  template<class T2, int N2, class F1, class F2>
  friend class ndarray;
  template<class T2, int N2, class G, class S>
  friend struct index_slicer;
  friend T *UPCXXA_PACK<T, N>(void *copy_desc_ptr,
                              size_t *array_data_size,
                              void *allocated);
  friend void UPCXXA_UNPACK<T, N>(UPCXXA_COPY_DESCRIPTOR<T, N> *copy_desc_ptr,
                                  T *buf);

 private:
  /* NOTE: the following TiArray methods rely on this field ordering:
      ti_cast_to_local, ti_cast_to_global, COPY_METHOD
   */
  UPCXXA_PTR_TO_T A;         /* pointer comes first for optimal struct
                                packing and access performance */
  UPCXXA_PTR_TO_T ancestor;  /* pointer to the start of the memory
                                block returned by malloc which
                                contains this array (this should be
                                freed by the destructor) */

  rectdomain<N> _domain;     /* The domain of the array. */

  cint_t stride[N];          /* The stride for index calc. Must be >=
                                1. */
  cint_t base[N];            /* The lower bound of domain. */
  cint_t side_factors[N];    /* Used in the address calculation. */

#if UPCXXA_EXPLICITLY_STORE_CREATOR
  int _creator;
#endif
};

/*
 * Array desc. size breakdown (bytes)          LOCAL           GLOBAL
 * ----------------------------------      ILP32   LP64    ILP32   LP64
 * 2*sizeof(PTR_TO_T) + // A, ancestor   =   8      16       16     32
 * 3*4*N + // rectdomain                 =      16N             16N
 * 3*4*N + // stride, base, side_factors =      12N             12N
 * (optional extras: 4)                  = (12)    (20)     (12)   (20)
 * TOTAL (default config - no extras):    8+28N  16+28N   16+28N 32+28N
 *                                 1D:      36      44       44     60
 *                                 2D:      64      72       72     88
 *                                 3D:      92     100      100    116
 */

} // namespace upcxx

#include "array_impl.tpp"
