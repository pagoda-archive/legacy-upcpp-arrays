#pragma once

#include <cstring>
#include <cstdio>
#include <cstdlib>
#ifdef UPCXXA_USE_CXX11
# include <type_traits>
#endif

#ifndef UPCXXA_EXPLICITLY_STORE_CREATOR
# define UPCXXA_EXPLICITLY_STORE_CREATOR 0
#endif

#define UPCXXA_DEREF_LOCAL(val, ptr)		do { (val) = *(ptr); } while (0)
#define UPCXXA_ASSIGN_LOCAL(ptr, val)		do { *(ptr) = (val); } while (0)
#define UPCXXA_WEAK_ASSIGN_LOCAL		UPCXXA_ASSIGN_LOCAL
#define UPCXXA_INDEX_LOCAL(element, base, offset)       \
  ((element) = (base) + (offset))

#ifndef UPCXXA_MEMORY_DISTRIBUTED
# include "array_defs_shmem.h"
#endif

#include "point.h"
#include "rdomain.h"
#include "domain.h"

#ifndef UPCXXA_UNINITIALIZED_CHECKING
# if UPCXXA_BOUNDS_CHECKING
#  define UPCXXA_UNINITIALIZED_CHECKING 1
# endif
#endif

#if UPCXXA_UNINITIALIZED_CHECKING
# define UPCXXA_ASSERT_EXISTSF(x, conv, where) do {             \
  UPCXXA_if_pf (conv((x).A) == NULL) {                          \
    fprintf(stderr, "fatal error on rank %i:\n"                 \
            "attempt to use uninitialized array at %s\n",       \
            UPCXXA_MYPROC, where);                              \
    fflush(stderr);                                             \
    abort();                                                    \
  } } while (0)
#else
# define UPCXXA_ASSERT_EXISTSF(x, conv, where) ((void)0)
#endif
#define UPCXXA_ASSERT_EXISTS(x, where)                  \
  UPCXXA_ASSERT_EXISTSF(x, UPCXXA_LOCAL_PART, where)

#define UPCXXA_GLOBAL_ARRAY_T global_tiarray
#define UPCXXA_LOCAL_ARRAY_T local_tiarray

#define UPCXXA_GLOBAL_ARRAY 0
#define UPCXXA_ARRAY_T UPCXXA_LOCAL_ARRAY_T
#define UPCXXA_PTR_TO_T T *
#define UPCXXA_REF_TO_T T &
#define UPCXXA_INDEX UPCXXA_INDEX_LOCAL
#define UPCXXA_LOCAL_PART(p) (p)
#define UPCXXA_BOX_PART(p)   UPCXXA_MYBOX
#define UPCXXA_EQUAL(ptr1,ptr2) ((ptr1) == (ptr2))
#define UPCXXA_DIRECTLY_ADDRESSABLE(ptr) (true)
#define UPCXXA_COPY_DESCRIPTOR local_copy_descriptor
#define UPCXXA_PACK local_pack
#define UPCXXA_UNPACK local_unpack
#define UPCXXA_DEBUG_ARRAYCOPY local_debugArrayCopy
#include "array_impl.h"
#undef UPCXXA_GLOBAL_ARRAY
#undef UPCXXA_ARRAY_T
#undef UPCXXA_PTR_TO_T
#undef UPCXXA_REF_TO_T
#undef UPCXXA_INDEX
#undef UPCXXA_LOCAL_PART
#undef UPCXXA_BOX_PART
#undef UPCXXA_EQUAL
#undef UPCXXA_DIRECTLY_ADDRESSABLE
#undef UPCXXA_COPY_DESCRIPTOR
#undef UPCXXA_PACK
#undef UPCXXA_UNPACK
#undef UPCXXA_DEBUG_ARRAYCOPY

#define UPCXXA_GLOBAL_ARRAY 1
#define UPCXXA_ARRAY_T UPCXXA_GLOBAL_ARRAY_T
#define UPCXXA_PTR_TO_T UPCXXA_GPTR_TO_T
#define UPCXXA_REF_TO_T UPCXXA_GREF_TO_T
#define UPCXXA_INDEX UPCXXA_INDEX_GLOBAL
#define UPCXXA_LOCAL_PART UPCXXA_TO_LOCAL
#define UPCXXA_BOX_PART   UPCXXA_TO_BOX
#define UPCXXA_EQUAL(ptr1,ptr2) UPCXXA_EQUAL_GLOBAL((ptr1),(ptr2))
#define UPCXXA_DIRECTLY_ADDRESSABLE(ptr) UPCXXA_IS_DIRECTLY_ADDRESSABLE(ptr)
#define UPCXXA_COPY_DESCRIPTOR global_copy_descriptor
#define UPCXXA_PACK global_pack
#define UPCXXA_UNPACK global_unpack
#define UPCXXA_DEBUG_ARRAYCOPY global_debugArrayCopy
#include "array_impl.h"
#undef UPCXXA_GLOBAL_ARRAY
#undef UPCXXA_ARRAY_T
#undef UPCXXA_PTR_TO_T
#undef UPCXXA_REF_TO_T
#undef UPCXXA_INDEX
#undef UPCXXA_LOCAL_PART
#undef UPCXXA_BOX_PART
#undef UPCXXA_EQUAL
#undef UPCXXA_DIRECTLY_ADDRESSABLE
#undef UPCXXA_COPY_DESCRIPTOR
#undef UPCXXA_PACK
#undef UPCXXA_UNPACK
#undef UPCXXA_DEBUG_ARRAYCOPY
#undef UPCXXA_BULK_READ
#undef UPCXXA_BULK_WRITE

#include "array_wrapper.h"

#include "array_undefs.h"
