// -------------------------------------------------------------------
//
// Name: neg_stride.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 990626
//
// Purpose:
//   Test the that using negative stride values generates an error
//
// Change Log:
//
// -------------------------------------------------------------------

// Test for negative strides

#include "../tests.h"

int main() {
  domain<1> D1 = rectdomain<1>(1, 10, -1);
  //No error on D2 - it's empty, so stride is irrelevant
  domain<1> D2 = rectdomain<1>(10, 1, -1);
  domain<2> D3 = rectdomain<2>(POINT(1,1), POINT(11,11), POINT(-1,1));
  //Ditto for D4 and 5
  domain<2> D4 = rectdomain<2>(POINT(10,10), POINT(2,0), POINT(1,-1));
  domain<2> D5 = rectdomain<2>(POINT(10,10), POINT(0,0), POINT(-1,-1));

  return 0;
}
