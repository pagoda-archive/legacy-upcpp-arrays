// Stencil using column-major arrays
#define UNSTRIDED , column
#define CMAJOR , true
#define SPLIT_LOOP
#define REV_ITER
#include "stencil.cpp"
