// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: kamil
//
// Date: 131119
//
// Purpose:
//   Sanity test for domains. Tests coversion from Domain temporary to
//   rectdomain.
//
// Change Log:
//
// -------------------------------------------------------------------

#include "../tests.h"

int main() {
  rectdomain<3> rd;
  domain<3> d, d2, derr;

  rd = rectdomain<3>(POINT(1,1,1), POINT(3,3,3), POINT(1,1,1));
  d = (domain<3>) rd;
  rd = (rectdomain<3>) (d + rectdomain<3>(POINT(1,1,1), POINT(4,4,4), POINT(1,1,1)));
  foreach (p, rd) {
    println("p = " << p);
  };

  return 0;
}
