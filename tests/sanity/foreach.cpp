// -------------------------------------------------------------------
//
// Name: foreach.tst
//
// Version: 1.0
//
// Creator: kamil
//
// Date: 131003
//
// Purpose:
//   Sanity test for domains. This tests the various foreach macros.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include <algorithm>
#include "../tests.h"

int main() {
  domain<3> d = rectdomain<3>(POINT(6,6,6), POINT(9,9,9)) +
    rectdomain<3>(POINT(2,2,2), POINT(5,5,5));
  point<3> *pts = new point<3>[d.size()];
  int idx = 0;
  foreach (p, d) { // unordered foreach over Domain
    pts[idx++] = p;
  };
  sort(pts, pts + d.size(), point<3>::compare); // sort points
  for (size_t i = 0; i < d.size(); i++) {
    point<3> p = pts[i];
    println("p = " << p);
  }
  println("-----");

  rectdomain<2> r = rectdomain<2>(POINT(5,5), POINT(9,10));
  foreach (p, r) { // foreach over rectdomain
    println("p = " << p);
  };
  println("-----");

  r = RD(PT(1, 1), PT(0, 0)); // empty
  foreach (p, r) {
    println("p = " << p);
  };
  println("-----");

  d = domain<3>(RD(PT(1, 1, 1), PT(0, 0, 0))); // empty
  foreach (p, d) {
    println("p = " << p);
  };

  return 0;
}
