// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

#include "../tests.h"

int main() {
  rectdomain<2> y;
  rectdomain<2> R = rectdomain<2>(POINT(1, 1), POINT(10, 20));
  point<2> mip = R.min(), mxp = R.max();
  int mi0 = mip[1], mx0 = mxp[1], mi1 = mip[2], mx1 = mxp[2];

  println("R = " << R);
  print("R.min() = (");
  print(mi0);
  print(", ");
  print(mi1);
  println(")");
  print("R.max() = (");
  print(mx0);
  print(", ");
  print(mx1);
  println(")");

  y = rectdomain<2>(POINT(1, 1), POINT(3, 5));
  println("All points in " << y);
  foreach (i, y) {
    print("(");
    print(i[1]);
    print(", ");
    print(i[2]);
    println(")");
  };
  y = rectdomain<2>(POINT(1, 1), POINT(3, 5));
  println("All points in " << y);
  foreach (i, y) {
    print("(");
    print(i[1]);
    print(", ");
    print(i[2]);
    println(")");
  };

  return 0;
}
