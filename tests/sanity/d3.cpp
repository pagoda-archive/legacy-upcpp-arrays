// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

#include "../tests.h"

int main() {
  rectdomain<3> rd;
  domain<3> d, d2, derr;

  rd = rectdomain<3>(POINT(1,1,1), POINT(3,3,3), POINT(1,1,1));
  d = (domain<3>) rd;
  foreach (p, d) {
    println("p = " << p);
  };
  println("---");
  rd = (rectdomain<3>) d;
  foreach (p, rd) {
    println("p = " << p);
  };
  println("---");
  derr = d + rectdomain<3>(POINT(2,2,2), POINT(4,4,4), POINT(1,1,1));
  d = d + rectdomain<3>(POINT(1,1,1), POINT(4,4,4), POINT(1,1,1));
  print("d is_rectangular: ");
  println_bool(d.is_rectangular());
  rd = (rectdomain<3>) d;
  foreach (p, rd) {
    println("p = " << p);
  };
  println("---");
  d2 = d + rectdomain<3>(POINT(2,2,2), POINT(4,4,4), POINT(1,1,1));
  print("d2 is_rectangular: ");
  println_bool(d2.is_rectangular());
  rd = (rectdomain<3>) d2;
  foreach (p, rd) {
    println("p = " << p);
  };
  println("---");
  print("derr is_rectangular: ");
  println_bool(derr.is_rectangular());
  // The next line should be a runtime error.
  rd = (rectdomain<3>) derr;
/*foreachn (p, rd, 3) {
    println("p = " << p);
  }
*/

  return 0;
}
