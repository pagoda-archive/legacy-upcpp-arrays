#include "arraytests.h"

static void dump(const char *s, ndarray<int, 3, strided> x) {
  foreach (p, x.domain()) {
    cout << s << "[" << p << "] = " << x[p] << endl;
  };
}

int main() {
  ndarray<int, 3> x(RD(PT(0, 0, 0), PT(3, 4, 5)));
  ndarray<int, 3, unstrided> y = x.permute(POINT(3, 2, 1)); /* invertable */
  ndarray<int, 3, unstrided> z = x.permute(POINT(2, 1, 3)); /* invertable */
  ndarray<int, 3, unstrided> w = x.permute(POINT(2, 3, 1)); /* not invertable */
    
  // cout << x.domain() << endl;
  // cout << y.domain() << endl;
  // cout << z.domain() << endl;
  // cout << w.domain() << endl;

  foreach (p, x.domain()) {
    x[p] = p[1] * 100 + p[2] * 10 + p[3];
  };

  dump("x", x);
  dump("y", y);
  dump("z", z);
  dump("w", w);

  return 0;
}
