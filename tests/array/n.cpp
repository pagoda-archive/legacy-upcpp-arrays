#include "arraytests.h"

struct Complex {
  double re, im;
};

int main() {
  int i;
  ndarray<int, 3> a;
  ndarray<ndarray<int, 1>, 3> b;
  ndarray<ndarray<ndarray<int, 1>, 3>, 2> e;
  ndarray<int *, 2> j;
  // int [] [2d] k;

  ndarray<Complex, 3> c;
  rectdomain<3> R3(PT(0, 0, 0), PT(4, 4, 4));
  rectdomain<1> R1(5, 11);
  rectdomain<2> R2(PT(4, 8), PT(6, 10));
  a = ndarray<int, 3>(R3);
  print("a.domain() = ");
  println(a.domain());

  b = ndarray<ndarray<int, 1>, 3>(R3);
  foreach (p, b.domain()) { b[p] = ndarray<int, 1>(R1); };
  print("b.domain() = ");
  println(b.domain());
  foreach (k, b.domain()) {
    print("b");
    print(k);
    print(".domain() = ");
    println(b[k].domain());
  };
    
  c = ndarray<Complex, 3>(R3);
  ndarray<ndarray<Complex, 3>, 1> c2 = ndarray<ndarray<Complex, 3>, 1>(R1);
  foreach (p, c2.domain()) { c2[p] = ndarray<Complex, 3>(R3); };
  c = c2[R1.min()];

  e = ndarray<ndarray<ndarray<int, 1>, 3>, 2>(R2);
  foreach (p, e.domain()) {
    e[p] = ndarray<ndarray<int, 1>, 3>(R3);
    foreach (q, e[p].domain()) {
      e[p][q] = ndarray<int, 1>(RD(1, 3));
    };
  };
  foreach (p, e.domain()) {
    print("Setting b to e");
    print(p);
    b = e[p];

    print("\nb.domain() = ");
    println(b.domain());
    foreach (k, b.domain()) {
      print("b");
      print(k);
      print(".domain() = ");
      println(b[k].domain());
    };
  };

  j = ndarray<int *, 2>(R2);
  foreach (p, j.domain()) { j[p] = new int[3]; };
  foreach (p, j.domain()) {
    print("j");
    print(p);
    print(".length = ");
    println(3/*j[p].length*/);
  };

  /*
    k = new int[3][R2];
    for (i = 0; i < 3; i++) {
    print("k[");
    print(i);
    print("]");
    print(".domain() = ");
    println(k[i].domain());
    }
  */

  return 0;
}
