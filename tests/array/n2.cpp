#include "arraytests.h"

struct Complex {
  double re, im;
};

static void printP1(point<1> p) {
  // print("[");
  // print(p[1]);
  // print("]");
}

static void printlnRD1(rectdomain<1> R) {
  // print("[");
  printP1(R.min());
  // print(" : ");
  printP1(R.max());
  if (R.stride() > POINT(1)) {
    // print(" : ");
    printP1(R.stride());
  }
  // println("]");
}

static void printP2(point<2> p) {
  // print("[");
  // print(p[1]);
  // print(", ");
  // print(p[2]);
  // print("]");
}

static void printlnRD2(rectdomain<2> R) {
  // print("[");
  printP2(R.min());
  // print(" : ");
  printP2(R.max());
  if (R.stride() > POINT(1, 1)) {
    // print(" : ");
    printP2(R.stride());
  }
  // println("]");
}

static void printP3(point<3> p) {
  // print("[");
  // print(p[1]);
  // print(", ");
  // print(p[2]);
  // print(", ");
  // print(p[3]);
  // print("]");
}

static void printlnRD3(rectdomain<3> R) {
  // print("[");
  printP3(R.min());
  // print(" : ");
  printP3(R.max());
  if (R.stride() > POINT(1, 1, 1)) {
    // print(" : ");
    printP3(R.stride());
  }
  // println("]");
}

int main() {
  int i;
  ndarray<int, 3> a;
  ndarray<ndarray<int, 1>, 3> b;
  ndarray<ndarray<ndarray<int, 1>, 3>, 2> e;
  ndarray<int *, 2> j;
  ndarray<int, 2> *k;

  ndarray<Complex, 3> c;
  rectdomain<3> R3(PT(0, 0, 0), PT(4, 4, 4));
  rectdomain<1> R1(5, 11);
  rectdomain<2> R2(PT(4, 8), PT(6, 10));
  a = ndarray<int, 3>(R3);
  // print("a.domain() = ");
  printlnRD3(a.domain());

  b = ndarray<ndarray<int, 1>, 3>(R3);
  foreach (p, b.domain()) { b[p] = ndarray<int, 1>(R1); };
  // print("b.domain() = ");
  printlnRD3(b.domain());
  foreach (m, b.domain()) {
    // print("b");
    printP3(m);
    // print(".domain() = ");
    printlnRD1(b[m].domain());
  };
    
  c = ndarray<Complex, 3>(R3);
  ndarray<ndarray<Complex, 3>, 1> c2 = ndarray<ndarray<Complex, 3>, 1>(R1);
  foreach (p, c2.domain()) { c2[p] = ndarray<Complex, 3>(R3); };
  c = c2[R1.min()];

  e = ndarray<ndarray<ndarray<int, 1>, 3>, 2>(R2);
  foreach (p, e.domain()) {
    e[p] = ndarray<ndarray<int, 1>, 3>(R3);
    foreach (q, e[p].domain()) {
      e[p][q] = ndarray<int, 1>(RD(1, 3));
    };
  };
  foreach (p, e.domain()) {
    // print("Setting b to e");
    printP2(p);
    b = e[p];

    // print("\nb.domain() = ");
    printlnRD3(b.domain());
    foreach (m, b.domain()) {
      // print("b");
      printP3(m);
      // print(".domain() = ");
      printlnRD1(b[m].domain());
    };
  };

  j = ndarray<int *, 2>(R2);
  foreach (p, j.domain()) { j[p] = new int[3]; };
  foreach (p, j.domain()) {
    // print("j");
    printP2(p);
    // print(".length = ");
    // println(j[p].length);
  };

  k = new ndarray<int, 2>[3];
  for (i = 0; i < 3; i++) k[i] = ndarray<int, 2>(R2);
  for (i = 0; i < 3; i++) {
    // print("k[");
    // print(i);
    // print("]");
    // print(".domain() = ");
    printlnRD2(k[i].domain());
  }

  println("ok");
  return 0;
}
