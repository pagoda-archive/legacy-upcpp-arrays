#include "arraytests.h"
#include <ctime>

int main(int argc, char **argv) {
  int output = 0;
  int S = 128;
  int iters = 10;
  if (argc > 1) output = atoi(argv[1]);
  if (argc > 2) S = atoi(argv[2]);
  if (argc > 3) iters = atoi(argv[3]);
  cout << "3-D Transpose test: sz=" << POINT(S,S,S) <<
    "(" << (S*S*S) << " elements, " << (S*S*S*8) << " bytes)  iters=" << iters << endl;
  ndarray<double, 3> src(RD(PT(0, 0, 0), PT(S, S, S)));
  ndarray<double, 3> dst(RD(PT(0, 0, 0), PT(S, S, S)));
  typedef ndarray<double, 3> double3d;
  ndarray<ndarray<double, 3, unstrided>, 1> srcs(RD(0, 6));
  ndarray<ndarray<double, 3, unstrided>, 1> dsts(RD(0, 6));
  ndarray<point<3>, 1> ps(RD(0, 6));
  ps[0] = POINT(1,2,3);
  ps[1] = POINT(1,3,2);
  ps[2] = POINT(2,1,3);
  ps[3] = POINT(2,3,1);
  ps[4] = POINT(3,1,2);
  ps[5] = POINT(3,2,1);
  foreach (i, ps.domain()) {
    srcs[i] = src.permute(ps[i]);
    dsts[i] = dst.permute(ps[i]);
  };
  foreach (p, src.domain()) {
    src[p] = 1000000*p[1] + 1000*p[2] + p[3];
  };

  foreach (i, ps.domain()) {
    foreach (j, ps.domain()) {
      clock_t t;
  
      dst.set(0); 
      t = clock();
      for (int x=0;x<iters;x++) {
        dsts[j].copy(srcs[i]);
      }
      t = clock() - t;
      cout << "copy/transpose " << ps[i] << "->" << ps[j] << ": ";
      if (output) cout << (((double) t)*1000/CLOCKS_PER_SEC/iters) << " ms/iter" << endl;
      else cout << "<output suppressed>" << endl;

      foreach (p, dsts[j].domain()) {
        if (srcs[i][p] != dsts[j][p]) {
          cout << "VERIFICATION FAILED: srcs[" << i << "]" << p << "=" << srcs[i][p] <<
            "  dsts["<< i << "]" << p << "=" << dsts[i][p] << endl;
        }
      };
    };
  };
          
}
