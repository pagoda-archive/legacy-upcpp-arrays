/* Test padding of arrays */
#include "arraytests.h"

#ifndef ArrayType
# define ArrayType ndarray
#endif
#ifndef GLOBAL
# define GLOBAL
#endif

#if __cplusplus >= 201103L
// test passing raw initializer lists to array ops
# define POINTLIT(...) { __VA_ARGS__ }
#else
# define POINTLIT POINT
#endif

int main() {
  cout << "Running array operations tests.." << endl;
  point<2> padding2 = {{ 1, 2 }};
  point<3> padding3 = {{ 1, 2, 3 }};
  /* test element location */
  { // sf[0] = 5+2
    ArrayType<int, 2, row GLOBAL> x(RD(PT(0, 0), PT(5, 5)), false, padding2);
    point<2> p1s[2] = { PT(2, 2), PT(2, 2) };
    point<2> p2s[2] = { PT(2, 3), PT(4, 3) };
    int expected[2] = { 1, 1+2*(5+2) };
    for (int i = 0; i < 2; i++) {
      int diff = (int) ((&x[p2s[i]]) - (&x[p1s[i]]));
      if (diff != expected[i])
      cout << "Unexpected offset between " << p2s[i] <<
        " and " << p1s[i] <<
        ": expected: " << expected[i] <<
        ": got: " << diff << endl;
    }
  }
  { // sf[1] = 3+3, sf[0]=3*sf[1]+2 = 3*(3+3)+2
    // slice(1, 1): x x x . . .
    //              x x x . . .
    //              x x x . . .
    //              _ _
    // slice(1, 2): x x x . . .
    //              x x x . . .
    //              x x x . . .
    //              _ _
    // slice(1, 3): x x x . . .
    //              x x x . . .
    //              x x x . . .
    //              _ _
    ArrayType<int, 3, strided GLOBAL> x(RD(PT(0, 0, 0), PT(3, 3, 3)), false, padding3);
    point<3> p1s[3] = { PT(0, 0, 0), PT(0, 0, 0), PT(0, 0, 0) };
    point<3> p2s[3] = { PT(0, 0, 1), PT(0, 2, 1), PT(2, 2, 1) };
    int expected[3] = { 1, 1+2*(3+3), 1+2*(3+3)+2*(3*(3+3)+2) };
    for (int i = 0; i < 3; i++) {
      int diff = (int) ((&x[p2s[i]]) - (&x[p1s[i]]));
      if (diff != expected[i])
      cout << "Unexpected offset between " << p2s[i] <<
        " and " << p1s[i] <<
        ": expected: " << expected[i] <<
        ": got: " << diff << endl;
    }
  }
  { // sf[1] = 3+1, sf[2]=3*sf[1]+2 = 3*(3+1)+2
    // slice(3, 1): x x x _
    //              x x x _
    //              x x x 
    //              . . .
    // slice(3, 2): x x x _
    //              x x x _
    //              x x x 
    //              . . .
    // slice(3, 3): x x x _
    //              x x x _
    //              x x x 
    //              . . .
    ArrayType<int, 3, unstrided GLOBAL> x(RD(PT(0, 0, 0), PT(3, 3, 3)), true, padding3);
    point<3> p1s[3] = { PT(0, 0, 0), PT(0, 0, 0), PT(0, 0, 0) };
    point<3> p2s[3] = { PT(1, 0, 0), PT(1, 2, 0), PT(1, 2, 2) };
    int expected[3] = { 1, 1+2*(3+1), 1+2*(3+1)+2*(3*(3+1)+2) };
    for (int i = 0; i < 3; i++) {
      int diff = (int) ((&x[p2s[i]]) - (&x[p1s[i]]));
      if (diff != expected[i])
      cout << "Unexpected offset between " << p2s[i] <<
        " and " << p1s[i] <<
        ": expected: " << expected[i] <<
        ": got: " << diff << endl;
    }
  }

  /* some basic array copy tests */
  { /* contiguous copy with differing base */
    ArrayType<int, 2, unstrided GLOBAL> x(RD(PT(1, 1), PT(101, 2)), false, padding2);
    ArrayType<int, 2, column GLOBAL> y(RD(PT(50, 1), PT(151, 2)), true, padding2);
    foreach (p, x.domain()) { x[p] = p[1]; };
    foreach (p, y.domain()) { y[p] = p[1]+1000; };
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 50) expected = p[1];
      else expected = p[1]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 1 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }
 
  { /* tranpose from contiguous -> contiguous */
    ArrayType<int, 2, row GLOBAL> x(RD(PT(1, 11), PT(6, 21)), false, padding2);
    ArrayType<int, 2, unstrided GLOBAL> y = ArrayType<int, 2, row GLOBAL>(RD(PT(11, 1), PT(21, 6)), false).permute(POINTLIT(2,1));
    foreach (p, x.domain()) { x[p] = p[1]*100+p[2]; };
    foreach (p, y.domain()) { y[p] = p[1]*100+p[2]+1000; };
    x.copy(y);
    foreach (p, x.domain()) {
      int expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 2 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* equal sideFactor/stride ratio */
    ArrayType<int, 2, strided GLOBAL> x(RD(PT(1, 11), PT(6, 21), PT(2, 1)), false, padding2);
    ArrayType<int, 2, unstrided GLOBAL> y(RD(PT(1, 11), PT(6, 16)), false);
    foreach (p, x.domain()) { x[p] = p[1]*100+p[2]; };
    foreach (p, y.domain()) { y[p] = p[1]*100+p[2]+1000; };
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[2] > 15) expected = p[1]*100+p[2];
      else expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 3 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  {
    // test overlap (contiguous)
    ArrayType<int, 3, unstrided GLOBAL> x(RD(PT(0, 0, 0), PT(7, 2, 2)), true, padding3);
    foreach (p, x.domain()) { x[p] = 100+p[1]; };
    ArrayType<int, 3, unstrided GLOBAL> y = x.translate(POINTLIT(2, 0, 0));
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 2) expected = 100+p[1];
      else expected = 100+p[1]-2;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy overlap test 4 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  {
    // test overlap (non-contiguous)
    ArrayType<int, 3, row GLOBAL> x(RD(PT(0, 0, 0), PT(61, 2, 2)), padding3);
    x = x.constrict(RD(PT(0, 0, 0), PT(61, 1, 1), PT(10, 1, 1)));
    foreach (p, x.domain()) { x[p] = 100+p[1]; };
    ArrayType<int, 3, row GLOBAL> y = x.translate(POINTLIT(20, 0, 0));
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 20) expected = 100+p[1];
      else expected = 100+p[1]-20;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy overlap test 5 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  cout << "done." << endl;
  return 0;
}
