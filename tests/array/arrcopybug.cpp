#include "arraytests.h"

int main() {
  ndarray<int, 1, strided> a1(RD(1, 11, 2));
  ndarray<int, 1, strided> a2(RD(2, 12, 2));
  ndarray<int, 1, strided> aback(RD(1, 11, 2));
  foreach (p, a1.domain()) { a1[p] = p[1]; };
  foreach (p, a2.domain()) { a2[p] = p[1]; };
  aback.copy(a1);
  a1.copy(a2);
  foreach (p, a1.domain()) { 
    if (a1[p] != aback[p])
      cout << "bad change at " << p << ": " << a1[p] << endl;
  };
  cout << "done." << endl;
  return 0;
}
