// -------------------------------------------------------------------
//
// Name: to_domain.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Test R.to_domain
//
// Change Log:
//
// -------------------------------------------------------------------

// Test to_domain from a rectdomainList or a PointList

#include "../tests.h"

int main() {
  domain<3> d1 = rectdomain<3>(point<3>::all(0), point<3>::all(0));
  rectdomain<3> *da1;
  point<3> *pa1;
  size_t da1s, pa1s;

  d1 = d1 + rectdomain<3>(point<3>::all(1), point<3>::all(4));
  d1 = d1 + rectdomain<3>(point<3>::all(2), point<3>::all(5));
  d1 = d1 + rectdomain<3>(point<3>::all(3), point<3>::all(6));

  da1 = d1.rectdomain_list();
  da1s = d1.num_rectdomains();
  pa1 = d1.point_list();
  pa1s = d1.size();

  domain<3> d2 = domain<3>::to_domain(da1, da1s);
  domain<3> d3 = domain<3>::to_domain(pa1, pa1s);

  if (!d2.equals(d3) || !d1.equals(d2) || !d1.equals(d3)) {
    println("Error. Domains do not match.");
  }

  return 0;
}
