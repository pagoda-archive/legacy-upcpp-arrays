#!/bin/bash 

if [ -n "${CXX}" ]
then
    cxx=${CXX}
else
    cxx=g++
fi

tests=($(ls */*.cpp | cut -d '.' -f 1))

rm -f a.out

for i in ${tests[@]}; do
    echo Running test ${i}...
    ${cxx} $* ${i}.cpp
    ./a.out | diff - ${i}.out
    rm -f a.out
done
