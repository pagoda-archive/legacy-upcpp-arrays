#include "../tests.h"

int main() {
  point<3> p = POINT(2, 3, 4);
  cout << p.permute(POINT(1, 2, 3)) << " =? (2,3,4)" << endl;
  cout << p.permute(POINT(1, 3, 2)) << " =? (2,4,3)" << endl;
  cout << p.permute(POINT(2, 1, 3)) << " =? (3,2,4)" << endl;
  cout << p.permute(POINT(2, 3, 1)) << " =? (3,4,2)" << endl;
  cout << p.permute(POINT(3, 1, 2)) << " =? (4,2,3)" << endl;
  cout << p.permute(POINT(3, 2, 1)) << " =? (4,3,2)" << endl;
  return 0;
}
