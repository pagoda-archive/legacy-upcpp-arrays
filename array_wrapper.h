#pragma once

/* array_wrapper.h
 * This provides the externally visible interface for multidimensional
 * arrays, the ndarray class, which is implemented as a wrapper around
 * the separate local and global array implementations ported from
 * Titanium. The ndarray class provides a more C++-like interface to
 * users.
 */

#include "wrapper_tools.h"

namespace upcxx {

  template<class T, int N, class F1 = arraydefault,
           class F2 = arraydefault>
  class ndarray {
  public:
    typedef UPCXXA_XGLOBALNESS(F1, F2) G;
    typedef UPCXXA_XSTRIDEDNESS(F1, F2) S;

    typedef array_impl_type<T, N, G> impl_type;
    typedef typename impl_type::arr_type arr_type;
    typedef typename impl_type::ptr_type ptr_type;
    typedef typename impl_type::ref_type ref_type;

    enum { arity = N };

    /* Implement UPC++ local array interface */
    typedef domain_utils::enable_if<UPCXXA_FLAG_EQUAL(G, local),
                                    T> local_elem_type;
    /* Implement UPC++ globalization interface */
    typedef domain_utils::enable_if<UPCXXA_FLAG_EQUAL(G, local),
                                    ndarray<T, N, global,
                                            S> > global_type;

    /* Constructor combinations:
     *  1. R
     *  2. R+cmajor
     *  3. R+alloc
     *  4. R+padding
     *  5. R+cmajor+padding
     *  6. R+cmajor+alloc
     *  7. R+alloc+padding
     *  8. R+cmajor+alloc+padding
     *  9. mem+R
     * 10. mem+R+cmajor
     * 11. mem+R+padding
     * 12. mem+R+cmajor+padding
     */

    /* 1, 2, 6, 8 */
    ndarray(const rectdomain<N> &R,
            bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N),
            void *(*allocator)(size_t) = UPCXXA_ARRAY_MALLOC,
            point<N> padding = point<N>::all(0)) {
      column_major = (N == 1) ? false : column_major;
      initialize((ptr_type) (T *) NULL, R, column_major, allocator,
                 padding);
    }

    /* 3, 7 */
    ndarray(const rectdomain<N> &R,
            void *(*allocator)(size_t),
            point<N> padding = point<N>::all(0)) {
      bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N);
      initialize((ptr_type) (T *) NULL, R, column_major, allocator,
                 padding);
    }

    /* 4 */
    ndarray(const rectdomain<N> &R,
            point<N> padding) {
      bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N);
      initialize((ptr_type) (T *) NULL, R, column_major,
                 UPCXXA_ARRAY_MALLOC, padding);
    }

    /* 5 */
    ndarray(const rectdomain<N> &R,
            bool column_major,
            point<N> padding) {
      column_major = (N == 1) ? false : column_major;
      initialize((ptr_type) (T *) NULL, R, column_major,
                 UPCXXA_ARRAY_MALLOC, padding);
    }

    /* Allow creation of array using externally allocated memory */
    /* 9, 10, 12 */
    ndarray(ptr_type mem, const rectdomain<N> &R,
            bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N),
            point<N> padding = point<N>::all(0)) {
      column_major = (N == 1) ? false : column_major;
      initialize(mem, R, column_major, NULL, padding);
    }

    /* 11 */
    ndarray(ptr_type mem, const rectdomain<N> &R,
            point<N> padding) {
      bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N);
      initialize(mem, R, column_major, NULL, padding);
    }

    ndarray() :
      _array(NULL), base((ptr_type) (T *) NULL),
      rebased((ptr_type) (T *) NULL) {}

    ndarray(void *) : // allow conversion from NULL
      _array(NULL), base((ptr_type) (T *) NULL),
      rebased((ptr_type) (T *) NULL) {}

    /* Allow implicit conversions to greater globalness and/or
     * stridedness */
    template<class G1, class G2>
    ndarray(const ndarray<T, N, G1, G2> &y,
            typename domain_utils::enable_if<UPCXXA_ALLOWICONV(F1, F2,
                                                               G1, G2),
            domain_utils::internal_type>::type =
            domain_utils::internal_type()) :
      _array((arr_type) y._array),
      base(impl_type::convert(y.base)),
      rebased(compute_rebased(_array)) {}

    /* Allow explicit conversions to lesser globalness and/or
     * stridedness */
    template<class G1, class G2>
    explicit ndarray(const ndarray<T, N, G1, G2> &y,
                     typename
                     domain_utils::enable_if<!UPCXXA_ALLOWICONV(F1, F2,
                                                                G1, G2),
                     domain_utils::internal_type>::type =
                     domain_utils::internal_type())  :
      _array((arr_type) y._array),
      base(impl_type::convert(y.base)),
      rebased(compute_rebased(_array)) {
#if UPCXXA_ARRAYS_USERCHECK
      if (!y.is_null()) {
        if (UPCXXA_FLAG_EQUAL(G, local) && !y.is_local()) {
          std::cerr << "failed to convert global array created by "
                    << y.creator() << " to local array on thread "
                    << UPCXXA_MYPROC << std::endl;
          abort();
        }
        if ((UPCXXA_FLAG_EQUAL(S, unstrided) && !y.is_unstrided()) ||
            (UPCXXA_FLAG_EQUAL(S, row) && !y.is_row()) ||
            (UPCXXA_FLAG_EQUAL(S, column) && !y.is_column())) {
          std::cerr << "failed to convert "
          << UPCXXA_STRIDEDNESS_STR(G1, G2)
          << " array created by "
          << y.creator() << " to "
          << UPCXXA_STRIDEDNESS_STR(F1, F2)
          << " array on thread "
          << UPCXXA_MYPROC << std::endl;
          abort();
        }
      }
#endif
    }

    /* Allow null arrays to be recreated after initial creation */ 
    void create(const rectdomain<N> &R,
                bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N),
                void *(*allocator)(size_t) = UPCXXA_ARRAY_MALLOC,
                point<N> padding = point<N>::all(0)) {
      column_major = (N == 1) ? false : column_major;
      initialize((ptr_type) (T *) NULL, R, column_major, allocator,
                 padding, true);
    }

    void create(const rectdomain<N> &R,
                void *(*allocator)(size_t),
                point<N> padding = point<N>::all(0)) {
      bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N);
      initialize((ptr_type) (T *) NULL, R, column_major, allocator,
                 padding, true);
    }

    void create(const rectdomain<N> &R,
                point<N> padding) {
      bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N);
      initialize((ptr_type) (T *) NULL, R, column_major,
                 UPCXXA_ARRAY_MALLOC, padding, true);
    }

    void create(const rectdomain<N> &R,
                bool column_major,
                point<N> padding) {
      column_major = (N == 1) ? false : column_major;
      initialize((ptr_type) (T *) NULL, R, column_major,
                 UPCXXA_ARRAY_MALLOC, padding, true);
    }

    void create(ptr_type mem, const rectdomain<N> &R,
                bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N),
                point<N> padding = point<N>::all(0)) {
      column_major = (N == 1) ? false : column_major;
      initialize(mem, R, column_major, NULL, padding, true);
    }

    void create(ptr_type mem, const rectdomain<N> &R,
                point<N> padding) {
      bool column_major = UPCXXA_DEFAULT_LAYOUT(S, N);
      initialize(mem, R, column_major, NULL, padding, true);
    }

    ndarray<T, UPCXXA_ARITY_N_MINUS_1,
            UPCXXA_XFER_STRIDE(slice)> slice(int k, int j) const {
      return
        ndarray<T, UPCXXA_ARITY_N_MINUS_1,
                UPCXXA_XFER_STRIDE(slice)>(_array.slice(k, j));
    }

    /* Delete the underlying array. The array data must be local, but
     * the array itself need not be qualified as local. The user must
     * ensure that no views of the underlying array are still in use,
     * and that the provided deallocator is the appropriate one.
     */
    void destroy(void (*deallocator)(void *) = UPCXXA_ARRAY_FREE) {
      _array.destroy(deallocator);
    }

    void exchange(T w) const {
      _array.exchange(w);
    }

    void vbroadcast(int s) const {
      _array.vbroadcast(s);
    }

    void set(T w) const {
      _array.set(w);
    }

    template<class G1, class G2>
    void copy(const ndarray<T, N, G1, G2> &y) const {
      _array.copy(y._array);
    }

    template<class G1, class G2>
    void async_copy(const ndarray<T, N, G1, G2> &y,
                    UPCXXA_EVENT_T e = UPCXXA_DEFAULT_EVENT) const {
      return _array.async_copy(y._array, e);
    }

    template<class G1, class G2>
    void copy(const ndarray<T, N, G1, G2> &y,
              const upcxx::domain<N> &dom) const {
      _array.copy(y._array, dom);
    }

    template<class G1, class G2>
    void copy(const ndarray<T, N, G1, G2> &y,
              const rectdomain<N> &rdom) const {
      _array.copy(y._array, rdom);
    }

    template<class G1, class G2, class H1, class H2>
    void copy(const ndarray<T, N, G1, G2> &y,
              const ndarray<point<N>, 1, H1, H2> &ptArray) const {
      _array.copy(y._array, ptArray._array);
    }

    template<class G1, class G2, class H1, class H2>
    void scatter(const ndarray<T, 1, G1, G2> &packedArray,
                 const ndarray<point<N>, 1, H1, H2> &ptArray) const {
      _array.scatter(packedArray._array, ptArray._array);
    }

    template<class G1, class G2, class H1, class H2>
    void gather(const ndarray<T, 1, G1, G2> &packedArray,
                const ndarray<point<N>, 1, H1, H2> &ptArray) const {
      _array.gather(packedArray._array, ptArray._array);
    }

    ndarray<T, N,
            UPCXXA_XFER_STRIDE(translate)> translate(const point<N> &p) const {
      return ndarray<T, N,
                     UPCXXA_XFER_STRIDE(translate)>(_array.translate(p));
    }

    ndarray<T, N,
            UPCXXA_XFER_STRIDE(permute)> permute(const point<N> &p) const {
      return ndarray<T, N,
                     UPCXXA_XFER_STRIDE(permute)>(_array.permute(p));
    }

    ndarray<T, N,
            UPCXXA_XFER_STRIDE(inject)> inject(const point<N> &p) const {
      return ndarray<T, N,
                     UPCXXA_XFER_STRIDE(inject)>(_array.inject(p));
    }

    ndarray<T, N,
            UPCXXA_XFER_STRIDE(project)> project(const point<N> &p) const {
      return ndarray<T, N,
                     UPCXXA_XFER_STRIDE(project)>(_array.project(p));
    }

    bool is_contiguous() const {
      return _array.is_contiguous();
    }

    rectdomain<N> domain() const {
      return _array.domain();
    }

    ndarray constrict(const rectdomain<N> &R) const {
      return ndarray(_array.constrict(R), rebased);
    }

    ndarray shrink(cint_t k) const {
      return ndarray(_array.shrink(k), rebased);
    }

    ndarray shrink(cint_t k, int dir) const {
      return ndarray(_array.shrink(k, dir), rebased);
    }

    ndarray border(cint_t k, int dir, cint_t shift) const {
      return ndarray(_array.border(k, dir, shift), rebased);
    }

    ndarray border(cint_t k, int dir) const {
      return ndarray(_array.border(k, dir), rebased);
    }

    ndarray border(int dir) const {
      return ndarray(_array.border(dir), rebased);
    }

    size_t size() const {
      return _array.size();
    }

    bool is_empty() const {
      return _array.is_empty();
    }

    int creator() const {
      return _array.creator();
    }

    UPCXXA_BOX_T where() const {
      return _array.where();
    }

    bool is_local() const {
      return UPCXXA_FLAG_EQUAL(G, local) || _array.is_local();
    }

    bool is_unstrided() const {
      if (UPCXXA_FLAG_EQUAL(S, unstrided) && !is_null()) {
        bool failure = false;
        for (int i = 0; i < N; i++) {
          if (_array.stride[i] != 1) {
            failure = true;
            break;
          }
        }
        return !failure;
      }
      return true;
    }

    bool is_row() const {
      if (!UPCXXA_FLAG_EQUAL(S, row) && !is_null()) {
        bool failure = _array.side_factors[N-1] != 1;
        for (int i = 0; i < N; i++) {
          if (_array.stride[i] != 1) {
            failure = true;
            break;
          }
        }
        return !failure;
      }
      return true;
    }

    bool is_column() const {
      if (!UPCXXA_FLAG_EQUAL(S, column) && !is_null()) {
        bool failure = _array.side_factors[0] != 1;
        for (int i = 0; i < N; i++) {
          if (_array.stride[i] != 1) {
            failure = true;
            break;
          }
        }
        return !failure;
      }
      return true;
    }

    /* Obtain a pointer to the underlying array memory. In bounds
     * checking mode, this method will fail if the underlying array is
     * not contiguous. */
    ptr_type storage_ptr() {
      return _array.storage_ptr();
    }

    const ptr_type storage_ptr() const {
      return _array.storage_ptr();
    }

    /* Obtain a pointer to the underlying array memory. This method
     * will always succeed. It is the user's responsibility to
     * correctly use the pointer, such as if the underlying array is
     * not contiguous. */
    ptr_type base_ptr() {
      return base;
    }

    const ptr_type base_ptr() const {
      return base;
    }

    bool is_null() const {
      return _array.is_null();
    }

    template<class G1, class G2>
    bool operator==(const ndarray<T, N, G1, G2> &y) const {
      return _array == y._array;
    }

    template<class G1, class G2>
    bool operator!=(const ndarray<T, N, G1, G2> &y) const {
      return !operator==(y);
    }

    bool operator==(void *y) const {
      return _array == y;
    }

    bool operator!=(void *y) const {
      return !operator==(y);
    }

    ref_type operator[](const point<N> &p) {
      return rebased[compute_index("operator[]", p)];
    }

    const ref_type operator[](const point<N> &p) const {
      return rebased[compute_index("operator[]", p)];
    }

    /* Allow integer indexing for 1D arrays */
    template<class U>
    typename
    domain_utils::enable_if<domain_utils::int_and_eq<U, N, 1>::value,
                            ref_type>::type
    operator[](U i) {
      return operator[](PT((cint_t) i));
    }

    template<class U>
    const typename
    domain_utils::enable_if<domain_utils::int_and_eq<U, N, 1>::value,
                            ref_type>::type
    operator[](U i) const {
      return operator[](PT(i));
    }

    ndarray<T, N-1, UPCXXA_XFER_STRIDE(islice)> slice(cint_t i) {
      return index_slicer<T, N, G, S>::slice(*this, i);
    }

    const ndarray<T, N-1, UPCXXA_XFER_STRIDE(islice)> slice(cint_t i) const {
      return
        index_slicer<T, N, G, S>::slice(*const_cast<ndarray<T, N,
                                        F1, F2> *>(this), i);
    }

#ifdef UPCXXA_USE_CXX11
    /* Indexing using variadic templates */
    template<class U, class... Is>
    typename
    domain_utils::enable_if<domain_utils::int_and_eq<U,
                            (N > UPCXXA_MAX_SPECIALIZED_ARITY),
                            1>::value, ref_type>::type
    operator()(U i1, Is... is) {
      static_assert(sizeof...(is) == (N-1),
                    "array of arity N must be indexed with N arguments");
      return
        rebased[var_indexer<N, S>::index(_array.base,
                                         _array.stride,
                                         _array.side_factors,
                                         UPCXXA_BC_PARAM(_array._domain)
                                         "operator()",
                                         i1, is...)];
    }

    template<class U, class... Is>
    const typename
    domain_utils::enable_if<domain_utils::int_and_eq<U,
                            (N > UPCXXA_MAX_SPECIALIZED_ARITY),
                            1>::value, ref_type>::type
    operator()(U i1, Is... is) const {
      static_assert(sizeof...(is) == (N-1),
                    "array of arity N must be indexed with N arguments");
      return
        rebased[var_indexer<N, S>::index(_array.base,
                                         _array.stride,
                                         _array.side_factors,
                                         UPCXXA_BC_PARAM(_array._domain)
                                         "operator()",
                                         i1, is...)];
    }
#endif

    /* operator() specializations */
#include "array_wrapper_specializations.h"

    template<class T2, int N2, class G1, class G2>
    friend class ndarray;
    template<class T2, int N2, class G2, class S2>
    friend struct index_slicer;

  private:
    static arr_type make_ti_array(ptr_type mem,
                                  const rectdomain<N> &R,
                                  bool column_major,
                                  void *(*allocator)(size_t),
                                  point<N> padding) {
      if (column_major) {
        point<N> p;
        for (int i = 0; i < N; i++) {
          p.x[i] = N-i;
        }
        return arr_type(R.permute(p), allocator, mem,
                        padding.permute(p)).permute(p);
      } else {
        return arr_type(R, allocator, mem, padding);
      }
    }

    static ptr_type compute_rebased(const arr_type &arr) {
#ifdef UPCXXA_NO_REBASE
      return arr.A;
#else
      if (UPCXXA_FLAG_EQUAL(S, strided)) {
        // Strided arrays can't be rebased, since the difference
        // between a point index and its base must be divided by the
        // stride, which may not divide the base itself.
        return arr.A;
      } else {
        long sum = 0;
        for (int i = N-1; i >= 0; i--) {
          sum -= arr.base[i] * arr.side_factors[i];
        }
        return arr.A + sum;
      }
#endif
    }

    void initialize(ptr_type mem, const rectdomain<N> &R,
                    bool column_major, void *(*allocator)(size_t),
                    point<N> padding, bool recreate = false) {
#if UPCXXA_ARRAYS_USERCHECK
      // Check that this not an invalid recreation
      if (recreate && !is_null()) {
        std::cerr << "create() can only be called on null arrays "
                  << "(thread = " << UPCXXA_MYPROC << ")"
                  << std::endl;
        abort();
      }
      // Check that flags correctly apply
      if (!R.is_empty()) {
        if (N > 1 && UPCXXA_FLAG_EQUAL(S, row) && column_major) {
          std::cerr << "row array of dimension " << N
                    << " > 1 cannot be column major (thread = "
                    << UPCXXA_MYPROC << ")" << std::endl;
          abort();
        }
        if (N > 1 && UPCXXA_FLAG_EQUAL(S, column) &&
            !column_major) {
          std::cerr << "column array of dimension " << N
                    << " > 1 cannot be row major (thread = "
                    << UPCXXA_MYPROC << ")" << std::endl;
          abort();
        }
        if (!UPCXXA_FLAG_EQUAL(S, strided) &&
            R.raw_stride() != point<N>::all(1)) {
          std::cerr << "cannot create unstrided array with strided "
                    << "domain " << R << ", (thread = "
                    << UPCXXA_MYPROC << ")" << std::endl;
          abort();
        }
      }
#endif
      _array = make_ti_array(mem, R, column_major, allocator,
                             padding);
      base = _array.A;
      rebased = compute_rebased(_array);
    }

    ndarray(const arr_type &arr) :
      _array(arr), base(arr.A), rebased(compute_rebased(arr)) {}

    ndarray(const arr_type &arr, const ptr_type &rb) :
      _array(arr), base(arr.A), rebased(rb) {}

    size_t compute_index(const char *where, const point<N> &p) const {
      UPCXXA_ASSERT_EXISTSF(_array, impl_type::localize, where);
      return
        rebased_indexer<N, S>::compute(_array.base, _array.stride,
                                       _array.side_factors,
                                       UPCXXA_BC_PARAM(_array._domain)
                                       where, p.x);
    }

    arr_type _array;      /* the underlying Titanium-style array */
    ptr_type base;        /* base pointer for orginal array data */
    ptr_type rebased;     /* rebased pointer for faster indexing */
  };

#ifdef UPCXXA_USE_CXX11
  /* Type alias for global arrays */
  template<class T, int N, class F = default_stridedness>
  using global_ndarray = ndarray<T, N, global, F>;
#endif

} // namespace upcxx
