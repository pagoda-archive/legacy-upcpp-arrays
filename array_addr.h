/* this file is included by array.cpp */

UPCXXA_ARRAYSIG(inline bool) operator==(const UPCXXA_ARRAY &y) const {
  bool const xnull = (UPCXXA_LOCAL_PART(A) == NULL);
  bool const ynull = (UPCXXA_LOCAL_PART(y.A) == NULL);
  if (xnull) return ynull;
  else if (ynull) return 0;
  else {
  #if 0
    fprintf(stderr, "fatal error on rank %i:\n"
            "attempted to compare two non-null grids\n", UPCXXA_MYPROC);
    fflush(stderr);
    abort();
  #else
    return UPCXXA_EQUAL(A,y.A) && _domain == y._domain;
  #endif
  }
}

/* Address of arr[p] is:
 *    desc.A + ((p[1] - desc.base[0]) / desc.stride[0]) * desc.side_factors[0]
 *           + ((p[2] - desc.base[1]) / desc.stride[1]) * desc.side_factors[1]
 *           + ...
 */

#define UPCXXA_BIDX(m) domain_utils::static_min<N-1, m>::arity
#define UPCXXA_PIDX(m) domain_utils::static_min<N, m>::arity

#define UPCXXA_ENABLE_ARR_MSG(msg) msg
#define UPCXXA_DISABLE_ARR_MSG(msg)

#if UPCXXA_BOUNDS_CHECKING
# define UPCXXA_CHECK_BOUND(loc, p, q, lo, upb, s, _domain, ENABLE1, ENABLE2)  \
  if (UPCXXA_PREDICT_FALSE(s > 1 && ((q - lo) % s) != 0)) {             \
    std::cerr << "Array bounds violation on rank " << UPCXXA_MYPROC <<  \
    "\nbounds checking error at " << loc << ":\n  failed condition: ";  \
    ENABLE1(std::cerr << _domain << ".contains(" << make_point<N>(p)    \
            << ")");                                                    \
    ENABLE2(std::cerr << "[stride] " << s << " divides ([index] " <<    \
            q << " - [min] " << lo << ")");                             \
    std::cerr << std::endl;                                             \
    abort();                                                            \
    exit(-2);                                                           \
  }                                                                     \
  else if (UPCXXA_PREDICT_FALSE((q < lo) | (q >= upb))) {               \
    std::cerr << "Array bounds violation on rank " << UPCXXA_MYPROC <<  \
    "\nbounds checking error at " << loc << ":\n  failed condition: ";  \
    ENABLE1(std::cerr << _domain.min() << " <= " << make_point<N>(p)    \
            << " <= " << _domain.max());                                \
    ENABLE2(std::cerr << "[min] " << lo << " <= [index] " << q <<       \
            " <= [max] " << (upb-1));                                   \
    std::cerr << std::endl;                                             \
    abort();                                                            \
    exit(-2);                                                           \
  }
# define UPCXXA_BC_PARAM(param) param,
# define UPCXXA_BC_CODE(code) code
#else
# define UPCXXA_CHECK_BOUND(loc, p, q, lo, upb, s, _domain, ENABLE1, ENABLE2)
# define UPCXXA_BC_PARAM(param)
# define UPCXXA_BC_CODE(code)
#endif

#if !UPCXXA_GLOBAL_ARRAY
/* Default ND index computation, with bounds checking if enabled */
template<int N> struct index_computer {
  /* Compute index relative to non-rebased pointer */
  inline static size_t compute(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &_domain)
                               const char *where,
                               const cint_t px[N]) {
    int i;
    size_t sum = 0;
    for (i = N-1; i >= 0; i--) {
      const cint_t str = stride[i];
      const cint_t q = px[i];
      const cint_t qm = q - base[i];
      cint_t delta = qm * side_factors[i];

# if UPCXXA_BOUNDS_CHECKING
      /* bounds check */
      const cint_t lo = _domain.min().x[i];
      const cint_t upb = _domain.upb().x[i];
      const cint_t s = _domain.stride().x[i];
      UPCXXA_CHECK_BOUND(where, px, q, lo, upb, s, _domain,
                         UPCXXA_ENABLE_ARR_MSG,
                         UPCXXA_DISABLE_ARR_MSG);
      assert(("internal error: bad array descriptor",
              stride[i] == 1 || (qm % stride[i]) == 0));
# endif

      /* At this point, stride[i-1] should divide qm.  Of course, we only
         know for sure if bounds checking is on. */
      /* only do the expensive divide when necessary */
      if (UPCXXA_PREDICT_FALSE(str != 1)) delta /= str;
      sum += (size_t) delta;
    }
# if 0
    std::cerr << "conv(" << p << ") = " << sum << std::endl;
# endif
    return sum;
  }
};

# if !UPCXXA_BOUNDS_CHECKING
/* Carefully optimized, non-bounds-checking 1D, 2D, and 3D versions */
template<> struct index_computer<1> {
  inline static size_t compute(const cint_t base[1],
                               const cint_t stride[1],
                               const cint_t side_factors[1],
                               const char *where,
                               const cint_t px[1]) {
    size_t const delta0 = (px[0] - base[0])*side_factors[0];
    return UPCXXA_PFAST_DIVIDE(delta0,stride[0]);
  }
};

template<> struct index_computer<2> {
  inline static size_t compute(const cint_t base[2],
                               const cint_t stride[2],
                               const cint_t side_factors[2],
                               const char *where,
                               const cint_t px[2]) {
    size_t const delta0 = (px[0] - base[0])*side_factors[0];
    size_t const delta1 = (px[1] - base[1])*side_factors[1];
    return UPCXXA_PFAST_DIVIDE(delta0,stride[0]) + 
      UPCXXA_PFAST_DIVIDE(delta1,stride[1]);
  }
};

template<> struct index_computer<3> {
  inline static size_t compute(const cint_t base[3],
                               const cint_t stride[3],
                               const cint_t side_factors[3],
                               const char *where,
                               const cint_t px[3]) {
    size_t const delta0 = (px[0] - base[0])*side_factors[0];
    size_t const delta1 = (px[1] - base[1])*side_factors[1];
    size_t const delta2 = (px[2] - base[2])*side_factors[2];
    return UPCXXA_PFAST_DIVIDE(delta0,stride[0]) + 
      UPCXXA_PFAST_DIVIDE(delta1,stride[1]) + 
      UPCXXA_PFAST_DIVIDE(delta2,stride[2]);
  }
};
# endif /* !UPCXXA_BOUNDS_CHECKING */
#endif /* !UPCXXA_GLOBAL_ARRAY */

#define UPCXXA__ABV_MSG1
#define UPCXXA__ABV_MSG2(arr) 

#define UPCXXA_ABV(arr, dim, created, printdomain, format, arg1, arg2, arg3) \
do {									 \
  fprintf(stderr, "Array bounds violation on rank %d!\n"                 \
	  "Error occurred in loop at %s in dimension %d of an array\n"	 \
	  "that" UPCXXA__ABV_MSG1 " has bounds ",                        \
          UPCXXA_MYPROC, (created), (dim) UPCXXA__ABV_MSG2(arr));        \
  printdomain((arr), stderr);						 \
  fputs(".\nCondition violated: ", stderr);				 \
  fprintf(stderr, (format), (arg1), (arg2), (arg3));			 \
  fputs("\n", stderr);							 \
  abort();								 \
  exit(-11);								 \
} while (0)


#define UPCXXA__FORALL_1(e,x,lo,hi,stride_,body,TPTR,INDEXFN,CVT)       \
  do {                                                                  \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)]*        \
                                        x.side_factors[UPCXXA_BIDX(0)], \
                                        x.stride[UPCXXA_BIDX(0)]);      \
    cint_t _i0=lo[UPCXXA_BIDX(0)];                                      \
    TPTR e;                                                             \
    INDEXFN(e,CVT(x.A),                                                 \
            UPCXXA_PFAST_DIVIDE((_i0-x.base[UPCXXA_BIDX(0)])*           \
                                x.side_factors[UPCXXA_BIDX(0)],         \
                                x.stride[UPCXXA_BIDX(0)]));             \
    for(;_i0<=hi[UPCXXA_BIDX(0)];_i0+=stride_[UPCXXA_BIDX(0)]) {        \
      body;                                                             \
      INDEXFN(e,e,ed);                                                  \
    }                                                                   \
  } while(0)

#define UPCXXA__FORALL_2(e,x,lo,hi,stride_,body,TPTR,INDEXFN,CVT)       \
  do {                                                                  \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)]*        \
                                        x.side_factors[UPCXXA_BIDX(1)], \
                                        x.stride[UPCXXA_BIDX(1)]);      \
    cint_t _i0=lo[UPCXXA_BIDX(0)];                                      \
    for(;_i0<=hi[UPCXXA_BIDX(0)];_i0+=stride_[UPCXXA_BIDX(0)]) {        \
      cint_t _i1=lo[UPCXXA_BIDX(1)];                                    \
      TPTR e;                                                           \
      INDEXFN(e,CVT(x.A),                                               \
              UPCXXA_PFAST_DIVIDE((_i1-x.base[UPCXXA_BIDX(1)])*         \
                                  x.side_factors[UPCXXA_BIDX(1)],       \
                                  x.stride[UPCXXA_BIDX(1)])+            \
              UPCXXA_PFAST_DIVIDE((_i0-x.base[UPCXXA_BIDX(0)])*         \
                                  x.side_factors[UPCXXA_BIDX(0)],       \
                                  x.stride[UPCXXA_BIDX(0)]));           \
      for(;_i1<=hi[UPCXXA_BIDX(1)];_i1+=stride_[UPCXXA_BIDX(1)]) {      \
        body;                                                           \
        INDEXFN(e,e,ed);                                                \
      }                                                                 \
    }                                                                   \
  } while(0)

#define UPCXXA__FORALL_3(e,x,lo,hi,stride_,body,TPTR,INDEXFN,CVT)       \
  do {                                                                  \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(2)]*        \
                                        x.side_factors[UPCXXA_BIDX(2)], \
                                        x.stride[UPCXXA_BIDX(2)]);      \
    cint_t _i0=lo[UPCXXA_BIDX(0)];                                      \
    for(;_i0<=hi[UPCXXA_BIDX(0)];_i0+=stride_[UPCXXA_BIDX(0)]) {        \
      cint_t _i1=lo[UPCXXA_BIDX(1)];                                    \
      for(;_i1<=hi[UPCXXA_BIDX(1)];_i1+=stride_[UPCXXA_BIDX(1)]) {      \
        cint_t _i2=lo[UPCXXA_BIDX(2)];                                  \
        TPTR e;                                                         \
        INDEXFN(e,CVT(x.A),                                             \
                UPCXXA_PFAST_DIVIDE((_i2-x.base[UPCXXA_BIDX(2)])*       \
                                    x.side_factors[UPCXXA_BIDX(2)],     \
                                    x.stride[UPCXXA_BIDX(2)])+          \
                UPCXXA_PFAST_DIVIDE((_i1-x.base[UPCXXA_BIDX(1)])*       \
                                    x.side_factors[UPCXXA_BIDX(1)],     \
                                    x.stride[UPCXXA_BIDX(1)])+          \
                UPCXXA_PFAST_DIVIDE((_i0-x.base[UPCXXA_BIDX(0)])*       \
                                    x.side_factors[UPCXXA_BIDX(0)],     \
                                    x.stride[UPCXXA_BIDX(0)]));         \
        for(;_i2<=hi[UPCXXA_BIDX(2)];_i2+=stride_[UPCXXA_BIDX(2)]) {    \
          body;                                                         \
          INDEXFN(e,e,ed);                                              \
        }                                                               \
      }                                                                 \
    }                                                                   \
  } while(0)

#define UPCXXA_COMPUTE_INDEX(idx,x,p,il)                                \
  do {                                                                  \
    idx = UPCXXA_PFAST_DIVIDE((il-x.base[N-1])*x.side_factors[N-1],     \
                              x.stride[N-1]);                           \
    for (int _k = N-2; _k >= 0; _k--)                                   \
      idx += UPCXXA_PFAST_DIVIDE((p[_k+1]-x.base[_k])*                  \
                                 x.side_factors[_k],x.stride[_k]);      \
  } while (0)

#define UPCXXA__FORALL_N(e,x_,lo,hi,stride_,body,TPTR,INDEXFN,CVT)      \
  do {                                                                  \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[N-1]*                   \
                                        x_.side_factors[N-1],           \
                                        x_.stride[N-1]);                \
    point<UPCXXA_ARITY_N_MINUS_1> _lo, _hi, _stride;                    \
    for (int _j = N-2; _j >= 0; _j--) {                                 \
      _lo.x[_j] = lo[_j];                                               \
      _hi.x[_j] = hi[_j] + 1;                                           \
      _stride.x[_j] = stride_[_j];                                      \
    }                                                                   \
    rectdomain<UPCXXA_ARITY_N_MINUS_1> _rd(_lo, _hi, _stride);          \
    upcxx_foreach (_p, _rd) {                                           \
      cint_t _il = lo[N-1];                                             \
      TPTR e;                                                           \
      cint_t _idx;                                                      \
      UPCXXA_COMPUTE_INDEX(_idx, x_, _p, _il);                          \
      INDEXFN(e, CVT(x_.A), _idx);                                      \
      for(;_il<=hi[N-1];_il+=stride_[N-1]) {                            \
        body;                                                           \
        INDEXFN(e,e,ed);                                                \
      }                                                                 \
    };                                                                  \
  } while(0)

#define UPCXXA__FORALL2_1(e,x,f,y,lo,hi,stride_,body,TPTR,INDEXFN,CVT)  \
  do {                                                                  \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)]*        \
                                        x.side_factors[UPCXXA_BIDX(0)], \
                                        x.stride[UPCXXA_BIDX(0)]);      \
    cint_t const fd=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)]*        \
                                        y.side_factors[UPCXXA_BIDX(0)], \
                                        y.stride[UPCXXA_BIDX(0)]);      \
    cint_t _i0=lo[UPCXXA_BIDX(0)];                                      \
    TPTR e;                                                             \
    TPTR f;                                                             \
    INDEXFN(e,CVT(x.A),                                                 \
            UPCXXA_PFAST_DIVIDE((_i0-x.base[UPCXXA_BIDX(0)])*           \
                                x.side_factors[UPCXXA_BIDX(0)],         \
                                x.stride[UPCXXA_BIDX(0)]));             \
    INDEXFN(f,CVT(y.A),                                                 \
            UPCXXA_PFAST_DIVIDE((_i0-y.base[UPCXXA_BIDX(0)])*           \
                                y.side_factors[UPCXXA_BIDX(0)],         \
                                y.stride[UPCXXA_BIDX(0)]));             \
    for(;_i0<=hi[UPCXXA_BIDX(0)];_i0+=stride_[UPCXXA_BIDX(0)]) {        \
      body;                                                             \
      INDEXFN(e,e,ed);                                                  \
      INDEXFN(f,f,fd);                                                  \
    }                                                                   \
  } while(0)

#define UPCXXA__FORALL2_2(e,x,f,y,lo,hi,stride_,body,TPTR,INDEXFN,CVT)  \
  do {                                                                  \
    cint_t _i0;                                                         \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)]*        \
                                        x.side_factors[UPCXXA_BIDX(1)], \
                                        x.stride[UPCXXA_BIDX(1)]);      \
    cint_t const fd=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)]*        \
                                        y.side_factors[UPCXXA_BIDX(1)], \
                                        y.stride[UPCXXA_BIDX(1)]);      \
    for(_i0=lo[UPCXXA_BIDX(0)];_i0<=hi[UPCXXA_BIDX(0)];                 \
        _i0+=stride_[UPCXXA_BIDX(0)]) {                                 \
      cint_t _i1=lo[UPCXXA_BIDX(1)];                                    \
      TPTR e;                                                           \
      TPTR f;                                                           \
      INDEXFN(e,CVT(x.A),                                               \
              UPCXXA_PFAST_DIVIDE((_i1-x.base[UPCXXA_BIDX(1)])*         \
                                  x.side_factors[UPCXXA_BIDX(1)],       \
                                  x.stride[UPCXXA_BIDX(1)])+            \
              UPCXXA_PFAST_DIVIDE((_i0-x.base[UPCXXA_BIDX(0)])*         \
                                  x.side_factors[UPCXXA_BIDX(0)],       \
                                  x.stride[UPCXXA_BIDX(0)]));           \
      INDEXFN(f,CVT(y.A),                                               \
              UPCXXA_PFAST_DIVIDE((_i1-y.base[UPCXXA_BIDX(1)])*         \
                                  y.side_factors[UPCXXA_BIDX(1)],       \
                                  y.stride[UPCXXA_BIDX(1)])+            \
              UPCXXA_PFAST_DIVIDE((_i0-y.base[UPCXXA_BIDX(0)])*         \
                                  y.side_factors[UPCXXA_BIDX(0)],       \
                                  y.stride[UPCXXA_BIDX(0)]));           \
      for(;_i1<=hi[UPCXXA_BIDX(1)];_i1+=stride_[UPCXXA_BIDX(1)]) {      \
        body;                                                           \
        INDEXFN(e,e,ed);                                                \
        INDEXFN(f,f,fd);                                                \
      }                                                                 \
    }                                                                   \
  } while(0)

#define UPCXXA__FORALL2_3(e,x,f,y,lo,hi,stride_,body,TPTR,INDEXFN,CVT)  \
  do {                                                                  \
    cint_t _i0;                                                         \
    cint_t _i1;                                                         \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(2)]*        \
                                        x.side_factors[UPCXXA_BIDX(2)], \
                                        x.stride[UPCXXA_BIDX(2)]);      \
    cint_t const fd=UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(2)]*        \
                                        y.side_factors[UPCXXA_BIDX(2)], \
                                        y.stride[UPCXXA_BIDX(2)]);      \
    for(_i0=lo[UPCXXA_BIDX(0)];_i0<=hi[UPCXXA_BIDX(0)];                 \
        _i0+=stride_[UPCXXA_BIDX(0)])                                   \
      for(_i1=lo[UPCXXA_BIDX(1)];_i1<=hi[UPCXXA_BIDX(1)];               \
          _i1+=stride_[UPCXXA_BIDX(1)]) {                               \
        cint_t _i2=lo[UPCXXA_BIDX(2)];                                  \
        TPTR e;                                                         \
        TPTR f;                                                         \
        INDEXFN(e,CVT(x.A),                                             \
                UPCXXA_PFAST_DIVIDE((_i2-x.base[UPCXXA_BIDX(2)])*       \
                                    x.side_factors[UPCXXA_BIDX(2)],     \
                                    x.stride[UPCXXA_BIDX(2)])+          \
                UPCXXA_PFAST_DIVIDE((_i1-x.base[UPCXXA_BIDX(1)])*       \
                                    x.side_factors[UPCXXA_BIDX(1)],     \
                                    x.stride[UPCXXA_BIDX(1)])+          \
                UPCXXA_PFAST_DIVIDE((_i0-x.base[UPCXXA_BIDX(0)])*       \
                                    x.side_factors[UPCXXA_BIDX(0)],     \
                                    x.stride[UPCXXA_BIDX(0)]));         \
        INDEXFN(f,CVT(y.A),                                             \
                UPCXXA_PFAST_DIVIDE((_i2-y.base[UPCXXA_BIDX(2)])*       \
                                    y.side_factors[UPCXXA_BIDX(2)],     \
                                    y.stride[UPCXXA_BIDX(2)])+          \
                UPCXXA_PFAST_DIVIDE((_i1-y.base[UPCXXA_BIDX(1)])*       \
                                    y.side_factors[UPCXXA_BIDX(1)],     \
                                    y.stride[UPCXXA_BIDX(1)])+          \
                UPCXXA_PFAST_DIVIDE((_i0-y.base[UPCXXA_BIDX(0)])*       \
                                    y.side_factors[UPCXXA_BIDX(0)],     \
                                    y.stride[UPCXXA_BIDX(0)]));         \
        for(;_i2<=hi[UPCXXA_BIDX(2)];_i2+=stride_[UPCXXA_BIDX(2)]) {    \
          body;                                                         \
          INDEXFN(e,e,ed);                                              \
          INDEXFN(f,f,fd);                                              \
        }                                                               \
      }                                                                 \
  } while(0)

#define UPCXXA__FORALL2_N(e,x_,f,y,lo,hi,stride_,body,TPTR,INDEXFN,CVT) \
  do {                                                                  \
    cint_t const ed=UPCXXA_PFAST_DIVIDE(stride_[N-1]*                   \
                                        x_.side_factors[N-1],           \
                                        x_.stride[N-1]);                \
    cint_t const fd=UPCXXA_PFAST_DIVIDE(stride_[N-1]*                   \
                                        y.side_factors[N-1],            \
                                        y.stride[N-1]);                 \
    point<UPCXXA_ARITY_N_MINUS_1> _lo, _hi, _stride;                    \
    for (int _j = N-2; _j >= 0; _j--) {                                 \
      _lo.x[_j] = lo[_j];                                               \
      _hi.x[_j] = hi[_j] + 1;                                           \
      _stride.x[_j] = stride_[_j];                                      \
    }                                                                   \
    rectdomain<UPCXXA_ARITY_N_MINUS_1> _rd(_lo, _hi, _stride);          \
    upcxx_foreach (_p, _rd) {                                           \
      cint_t _il = lo[N-1];                                             \
      TPTR e;                                                           \
      TPTR f;                                                           \
      cint_t _idx;                                                      \
      UPCXXA_COMPUTE_INDEX(_idx, x_, _p, _il);                          \
      INDEXFN(e, CVT(x_.A), _idx);                                      \
      UPCXXA_COMPUTE_INDEX(_idx, y, _p, _il);                           \
      INDEXFN(f, CVT(y.A), _idx);                                       \
      for(;_il<=hi[N-1];_il+=stride_[N-1]) {                            \
        body;                                                           \
        INDEXFN(e,e,ed);                                                \
        INDEXFN(f,f,fd);                                                \
      }                                                                 \
    };                                                                  \
 } while(0)

#if UPCXXA_GLOBAL_ARRAY
# define UPCXXA_FORALL_1(e,x,lo,hi,stride,body,localbody) do {          \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A))                            \
      UPCXXA__FORALL_1(e,x,lo,hi,stride,localbody,T*,                   \
                       UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);             \
    else                                                                \
      UPCXXA__FORALL_1(e,x,lo,hi,stride,body,UPCXXA_PTR_TO_T,           \
                       UPCXXA_INDEX,UPCXXA__IDENTITY);                  \
  } while(0)
# define UPCXXA_FORALL_2(e,x,lo,hi,stride,body,localbody) do {          \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A))                            \
      UPCXXA__FORALL_2(e,x,lo,hi,stride,localbody,T*,                   \
                       UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);             \
    else                                                                \
      UPCXXA__FORALL_2(e,x,lo,hi,stride,body,UPCXXA_PTR_TO_T,           \
                       UPCXXA_INDEX,UPCXXA__IDENTITY);                  \
  } while(0)
# define UPCXXA_FORALL_3(e,x,lo,hi,stride,body,localbody) do {          \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A))                            \
      UPCXXA__FORALL_3(e,x,lo,hi,stride,localbody,T*,                   \
                       UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);             \
    else                                                                \
      UPCXXA__FORALL_3(e,x,lo,hi,stride,body,UPCXXA_PTR_TO_T,           \
                       UPCXXA_INDEX,UPCXXA__IDENTITY);                  \
  } while(0)
# define UPCXXA_FORALL_N(e,x,lo,hi,stride,body,localbody) do {          \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A))                            \
      UPCXXA__FORALL_N(e,x,lo,hi,stride,localbody,T*,                   \
                       UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);             \
    else                                                                \
      UPCXXA__FORALL_N(e,x,lo,hi,stride,body,UPCXXA_PTR_TO_T            \
                       ,UPCXXA_INDEX,UPCXXA__IDENTITY);                 \
  } while(0)
# define UPCXXA_FORALL2_1(e,x,f,y,lo,hi,stride,body,localbody) do {     \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A) &&                          \
        UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A))                            \
      UPCXXA__FORALL2_1(e,x,f,y,lo,hi,stride,localbody,T*,              \
                        UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);            \
    else                                                                \
      UPCXXA__FORALL2_1(e,x,f,y,lo,hi,stride,body,UPCXXA_PTR_TO_T,      \
                        UPCXXA_INDEX,UPCXXA__IDENTITY);                 \
  } while(0)
# define UPCXXA_FORALL2_2(e,x,f,y,lo,hi,stride,body,localbody) do {     \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A) &&                          \
        UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A))                            \
      UPCXXA__FORALL2_2(e,x,f,y,lo,hi,stride,localbody,T*,              \
                        UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);            \
    else                                                                \
      UPCXXA__FORALL2_2(e,x,f,y,lo,hi,stride,body,UPCXXA_PTR_TO_T,      \
                        UPCXXA_INDEX,UPCXXA__IDENTITY);                 \
  } while(0)
# define UPCXXA_FORALL2_3(e,x,f,y,lo,hi,stride,body,localbody) do {     \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A) &&                          \
        UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A))                            \
      UPCXXA__FORALL2_3(e,x,f,y,lo,hi,stride,localbody,T*,              \
                        UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);            \
    else                                                                \
      UPCXXA__FORALL2_3(e,x,f,y,lo,hi,stride,body,UPCXXA_PTR_TO_T,      \
                        UPCXXA_INDEX,UPCXXA__IDENTITY);                 \
  } while(0)
# define UPCXXA_FORALL2_N(e,x,f,y,lo,hi,stride,body,localbody) do {     \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A) &&                          \
        UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A))                            \
      UPCXXA__FORALL2_N(e,x,f,y,lo,hi,stride,localbody,T*,              \
                        UPCXXA_INDEX_LOCAL,UPCXXA_TO_LOCAL);            \
    else                                                                \
      UPCXXA__FORALL2_N(e,x,f,y,lo,hi,stride,body,UPCXXA_PTR_TO_T,      \
                        UPCXXA_INDEX,UPCXXA__IDENTITY);                 \
  } while(0)
#else
# define UPCXXA_FORALL_1(e,x,lo,hi,stride,body,localbody)               \
  UPCXXA__FORALL_1(e,x,lo,hi,stride,localbody,T*,                       \
                   UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL_2(e,x,lo,hi,stride,body,localbody)               \
  UPCXXA__FORALL_2(e,x,lo,hi,stride,localbody,T*,                       \
                   UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL_3(e,x,lo,hi,stride,body,localbody)               \
  UPCXXA__FORALL_3(e,x,lo,hi,stride,localbody,T*,                       \
                   UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL_N(e,x,lo,hi,stride,body,localbody)       \
  UPCXXA__FORALL_N(e,x,lo,hi,stride,localbody,T*,               \
                   UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL2_1(e,x,f,y,lo,hi,stride,body,localbody)  \
  UPCXXA__FORALL2_1(e,x,f,y,lo,hi,stride,localbody,T*,          \
                    UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL2_2(e,x,f,y,lo,hi,stride,body,localbody)  \
  UPCXXA__FORALL2_2(e,x,f,y,lo,hi,stride,localbody,T*,          \
                    UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL2_3(e,x,f,y,lo,hi,stride,body,localbody)  \
  UPCXXA__FORALL2_3(e,x,f,y,lo,hi,stride,localbody,T*,          \
                    UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
# define UPCXXA_FORALL2_N(e,x,f,y,lo,hi,stride,body,localbody)  \
  UPCXXA__FORALL2_N(e,x,f,y,lo,hi,stride,localbody,T*,          \
                    UPCXXA_INDEX_LOCAL,UPCXXA__IDENTITY)
#endif // UPCXXA_GLOBAL_ARRAY

#define UPCXXA_FORALL(e, x, lo, hi, stride, body, localbody) do {       \
    if (N == 1)                                                         \
      UPCXXA_FORALL_1(e, x, lo, hi, stride, body, localbody);           \
    else if (N == 2)                                                    \
      UPCXXA_FORALL_2(e, x, lo, hi, stride, body, localbody);           \
    else if (N == 3)                                                    \
      UPCXXA_FORALL_3(e, x, lo, hi, stride, body, localbody);           \
    else                                                                \
      UPCXXA_FORALL_N(e, x, lo, hi, stride, body, localbody);           \
  } while (0)
  
#define UPCXXA_FORALL2(e, x, f, y, lo, hi, stride, body, localbody)     \
  do {                                                                  \
    if (N == 1)                                                         \
      UPCXXA_FORALL2_1(e, x, f, y, lo, hi, stride, body, localbody);    \
    else if (N == 2)                                                    \
      UPCXXA_FORALL2_2(e, x, f, y, lo, hi, stride, body, localbody);    \
    else if (N == 3)                                                    \
      UPCXXA_FORALL2_3(e, x, f, y, lo, hi, stride, body, localbody);    \
    else                                                                \
      UPCXXA_FORALL2_N(e, x, f, y, lo, hi, stride, body, localbody);    \
  } while (0)

  
#define UPCXXA__ARRAY_FASTADDR1(type, result, baseptr, ARR, pt) do {    \
    cint_t const delta0 =                                               \
      UPCXXA_PFAST_MULT((pt.x[UPCXXA_BIDX(0)] -                         \
                         ARR.base[UPCXXA_BIDX(0)]),                     \
                        ARR.side_factors[UPCXXA_BIDX(0)]);              \
    UPCXXA_INDEX_##type(result, baseptr,                                \
                        UPCXXA_PFAST_DIVIDE(delta0,                     \
                                            ARR.stride[UPCXXA_BIDX(0)])); \
  } while (0)
#define UPCXXA__ARRAY_FASTADDR2(type, result, baseptr, ARR, pt) do {    \
    cint_t const delta0 =                                               \
      (pt.x[UPCXXA_BIDX(0)] -                                           \
       ARR.base[UPCXXA_BIDX(0)])*ARR.side_factors[UPCXXA_BIDX(0)];      \
    cint_t const delta1 =                                               \
      UPCXXA_PFAST_MULT((pt.x[UPCXXA_BIDX(1)] -                         \
                         ARR.base[UPCXXA_BIDX(1)]),                     \
                        ARR.side_factors[UPCXXA_BIDX(1)]);              \
    UPCXXA_INDEX_##type(result, baseptr,                                \
                        UPCXXA_PFAST_DIVIDE(delta0,                     \
                                            ARR.stride[UPCXXA_BIDX(0)]) + \
                        UPCXXA_PFAST_DIVIDE(delta1,                     \
                                            ARR.stride[UPCXXA_BIDX(1)])); \
  } while (0)
#define UPCXXA__ARRAY_FASTADDR3(type, result, baseptr, ARR, pt) do {    \
    cint_t const delta0 =                                               \
      (pt.x[UPCXXA_BIDX(0)] -                                           \
       ARR.base[UPCXXA_BIDX(0)])*ARR.side_factors[UPCXXA_BIDX(0)];      \
    cint_t const delta1 =                                               \
      (pt.x[UPCXXA_BIDX(1)] -                                           \
       ARR.base[UPCXXA_BIDX(1)])*ARR.side_factors[UPCXXA_BIDX(1)];      \
    cint_t const delta2 =                                               \
      UPCXXA_PFAST_MULT((pt.x[UPCXXA_BIDX(2)] -                         \
                         ARR.base[UPCXXA_BIDX(2)]),                     \
                        ARR.side_factors[UPCXXA_BIDX(2)]);              \
    UPCXXA_INDEX_##type(result, baseptr,                                \
                        UPCXXA_PFAST_DIVIDE(delta0,                     \
                                            ARR.stride[UPCXXA_BIDX(0)]) + \
                        UPCXXA_PFAST_DIVIDE(delta1,                     \
                                     ARR.stride[UPCXXA_BIDX(1)]) +      \
                        UPCXXA_PFAST_DIVIDE(delta2,                     \
                                            ARR.stride[UPCXXA_BIDX(2)])); \
 } while (0)
#define UPCXXA__ARRAY_FASTADDRN(N, type, result, baseptr, ARR, pt) do { \
    cint_t _delta =                                                     \
      UPCXXA_PFAST_MULT((pt.x[N-1] - ARR.base[N-1]),                    \
                        ARR.side_factors[N-1]);                         \
    cint_t _idx = UPCXXA_PFAST_DIVIDE(_delta,ARR.stride[N-1]);          \
    for (int _i = 0; _i < N-1; _i++) {                                  \
      _delta = (pt.x[_i] - ARR.base[_i])*ARR.side_factors[_i];          \
      _idx += UPCXXA_PFAST_DIVIDE(_delta,ARR.stride[_i]);               \
    }                                                                   \
    UPCXXA_INDEX_##type(result, baseptr, _idx);                         \
 } while (0)
#define UPCXXA__ARRAY_FASTBEST1(type, result, preparedbase, ARR, pt)    \
  do {                                                                  \
    UPCXXA_INDEX_##type(result, preparedbase, pt.x[UPCXXA_BIDX(0)]);    \
  } while (0)
#define UPCXXA__ARRAY_FASTBEST2(type, result, preparedbase, ARR, pt)    \
  do {                                                                  \
    UPCXXA_INDEX_##type(result, preparedbase,                           \
                        pt.x[UPCXXA_BIDX(0)]*                           \
                        ARR.side_factors[UPCXXA_BIDX(0)] +              \
                        pt.x[UPCXXA_BIDX(1)]);                          \
  } while (0)
#define UPCXXA__ARRAY_FASTBEST3(type, result, preparedbase, ARR, pt)    \
  do {                                                                  \
    UPCXXA_INDEX_##type(result, preparedbase,                           \
                        pt.x[UPCXXA_BIDX(0)]*                           \
                        ARR.side_factors[UPCXXA_BIDX(0)] +              \
                        pt.x[UPCXXA_BIDX(1)]*                           \
                        ARR.side_factors[UPCXXA_BIDX(1)] +              \
                        pt.x[UPCXXA_BIDX(2)]);                          \
  } while (0)
#define UPCXXA__ARRAY_FASTBESTN(N, type, result, preparedbase, ARR, pt) \
  do {                                                                  \
    cint_t _idx = pt.x[N-1];                                            \
    for (int _i = 0; _i < N-1; _i++)                                    \
      _idx += pt.x[_i]*ARR.side_factors[_i];                            \
    UPCXXA_INDEX_##type(result, preparedbase, _idx);                    \
  } while (0)


#define UPCXXA_ARRAY_FASTCONV(N, result, base, ARR, pt) do {            \
    if (N == 1)                                                         \
      UPCXXA__ARRAY_FASTADDR1(LOCAL, result, base, ARR, pt);            \
    else if (N == 2)                                                    \
      UPCXXA__ARRAY_FASTADDR2(LOCAL, result, base, ARR, pt);            \
    else if (N == 3)                                                    \
      UPCXXA__ARRAY_FASTADDR3(LOCAL, result, base, ARR, pt);            \
    else                                                                \
      UPCXXA__ARRAY_FASTADDRN(N, LOCAL, result, base, ARR, pt);         \
  } while (0)
/*
  this macro is used by the scatter/gather methods in ti_array.c 
  to access the FASTBEST calculation
  it doesn't work for the general case
  it assumes that strides are all 1, and sidefactor[N-1] is 1
*/
#define UPCXXA_ARRAY_FASTBESTCONV(N, result, preparedbase, ARR, pt)     \
  do {                                                                  \
    if (N == 1)                                                         \
      UPCXXA__ARRAY_FASTBEST1(LOCAL, result, preparedbase, ARR, pt);    \
    else if (N == 2)                                                    \
      UPCXXA__ARRAY_FASTBEST2(LOCAL, result, preparedbase, ARR, pt);    \
    else if (N == 3)                                                    \
      UPCXXA__ARRAY_FASTBEST3(LOCAL, result, preparedbase, ARR, pt);    \
    else                                                                \
      UPCXXA__ARRAY_FASTBESTN(N, LOCAL, result, preparedbase, ARR, pt); \
  } while (0)
