#pragma once

#include "utils.h"
#include "iter.h"
#ifdef UPCXXA_USE_CXX11
# include <type_traits>
#endif

#ifndef UPCXXA_USE_CXX11
// Generic foreach over any domain. This makes assumptions about the
// implementation of points.
# define upcxx_foreach(p, dom)                                  \
  UPCXXA_FOREACH_(p, (dom),                                     \
                  (sizeof((dom).min().x) /                      \
                   sizeof(upcxx::cint_t)),                      \
                  UPCXXA_UNIQUIFYN(foreach_ptr_, p))
# define UPCXXA_FOREACH_(p, dom, N, ptr_)                       \
  for (upcxx::point_iter<N> ptr_ = (dom).iter(); !ptr_.done;    \
       ptr_.done = true)                                        \
    for (upcxx::point<N> p = ptr_.start(); ptr_.next(p);)
#else
// C++11 foreach that makes no implementation assumptions.
# define upcxx_foreach(p, dom)                                  \
  UPCXXA_FOREACH_(p, (dom), UPCXXA_UNIQUIFYN(foreach_ptr_, p))
# define UPCXXA_FOREACH_(p, dom, ptr_)                          \
  for (auto ptr_ = (dom).iter(); !ptr_.done; ptr_.done = true)  \
    ptr_ = [&](const decltype(ptr_.dummy_pt()) &p)
#endif

#ifdef UPCXXA_SHORT_MACROS
# define foreach upcxx_foreach
#endif
