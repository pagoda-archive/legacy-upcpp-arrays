//
// ===========
// Rect Domain
// ===========
//
// This represents a domain that looks like an N-dim rectangle,
// with sides parallel to the dimensional axes.  This is optimized
// for a fast "foreach" at the expense of set operations.

#include <stdexcept>
#include <cassert>
#include <cstdlib>
#include "utils.h"

#define UPCXXA_EMPTYREP(rd) do {                \
    rd->p0 = UPCXXA_ZERO_POINT;                 \
    rd->p1 = UPCXXA_ZERO_POINT;                 \
    rd->loop_stride = UPCXXA_NEGONE_POINT;      \
  } while (false)

//Is rectdomain obj singular or empty in dimension i?
#define UPCXXA_STRIDE_I_ZERO(obj,i)             \
  ((obj)->p0.get0(i)>=((obj)->p1.get0(i)-1))

namespace upcxx {

// Constructors
template<int N> inline rectdomain<N>::rectdomain() {
  UPCXXA_EMPTYREP(this);
}

template<int N> inline rectdomain<N>::rectdomain(const point<N> &set_p0,
                                                 const point<N> &set_p1) {
  if (!(set_p0 < set_p1)) {
    UPCXXA_EMPTYREP(this);
    return;
  }

  p0=set_p0;
  p1=set_p1;
  loop_stride=UPCXXA_ONES_POINT;
}

template<int N> inline void rectdomain<N>::normalize() {
  for (int i = 0; i < N; i++) {  
    if (loop_stride.get0(i) == 0) { // singular in i 
      p1 = p1.set0(i, p0.get0(i) + 1); 
      loop_stride = loop_stride.set0(i, 1);
    } else { 
      cint_t tmp =  domain_utils::align_down((p1.get0(i) -
                                              p0.get0(i)) - 1,
                                             loop_stride.get0(i)) + 1;
      p1 = p1.set0(i, p0.get0(i) + tmp); 
      if (loop_stride.get0(i) >= tmp) // singular in i 
        loop_stride = loop_stride.set0(i, 1);
    }
  }
}

template<int N> rectdomain<N>::rectdomain(const point<N> &set_p0,
                                          const point<N> &set_p1,
                                          const point<N> &set_stride) {
  // normalize 
  if (set_p0 < set_p1) { 
    // check stride is positive only for nonempty rectdomains
    UPCXXA_CHECK(set_stride>=UPCXXA_ZERO_POINT,
                 "stride may not contain a negative point<N> value.");
    // p0     
    // nothing necessary 
    // p1     

    p0 = set_p0;
    p1 = set_p1;
    loop_stride = set_stride;

    normalize();
  } else {
    // empty set
    UPCXXA_EMPTYREP(this);
    return;
  }
}

// THE FOLLOWING TWO CONSTRUCTORS ARE DANGEROUS
//   UNCHECKED ASSERTIONS:
//   (set_p0[i] < set_p1[i] for all i) || ( set_loop_stride.get0(0)<0 )
//   ^---nonempty                      or  ^----proper empty rep (only for ctor 2)
//   THE FIRST CONSTRUCTOR WILL NOT CONSTRUCT AN EMPTY RECTDOMAIN. DON'T MAKE IT TRY.
//   for the second constructor, if the points would make an empty rectdomain, then caller must ensure set_loop_stride == (new rectdomain<N>()).stride())
//                               if the RD is to be nonempty, caller must ensure that set_loop_stride>=point.all(1)
//   for the second constructor, the stride must be valid given set_p0 and set_p1 (i.e., set_p1 must be aligned WRT set_p0 and stride)
//                               that is, set_p1[i]=set_p0[i]+1+k*set_loop_stride[i] for some k>=0
//                               and if k==0, then set_loop_stride[i] must be 1

template<int N> inline rectdomain<N>::rectdomain(const point<N> &set_p0,
                                                 const point<N> &set_p1,
                                                 float fUnused) {
  assert((set_p0<set_p1)); //,"!p0<p1 in tiRD(p,p,b)");
  p0=set_p0;
  p1=set_p1;
  loop_stride=UPCXXA_ONES_POINT;
}

//SEE ABOVE FOR UNCHECKED ASSERTIONS
template<int N> inline rectdomain<N>::rectdomain(const point<N> &set_p0,
                                                 const point<N> &set_p1,
                                                 const point<N> &set_loop_stride,
                                                 float fUnused) {
  assert(((set_p0<set_p1)||(set_loop_stride.get0(0)<0)));
  p0=set_p0;
  p1=set_p1;
  loop_stride=set_loop_stride;
}

template<int N> inline rectdomain<N>::rectdomain(const rectdomain<N> &set_rd) {
  p0 = set_rd.p0;
  p1 = set_rd.p1;
  loop_stride = set_rd.loop_stride;
}

template<int N> rectdomain<N>::rectdomain(domain<N> &d) {
  if (d.is_empty()) {
    UPCXXA_EMPTYREP(this);
  } else {
    rectdomain<N> *arr=d.is_rectangular_ex();
    UPCXXA_CHECKF((arr!=NULL),"not a rectdomain in cast.",
                  {UPCXXA_EMPTYREP(this); return;});
    rectdomain<N> rd=arr[0]; 
    p0 = rd.p0;
    p1 = rd.p1;
    loop_stride = rd.loop_stride;
  }
}

template<int N> rectdomain<N>::rectdomain(const domain<N> &d) {
  if (d.is_empty()) {
    UPCXXA_EMPTYREP(this);
  } else {
    domain<N> nd(d);
    rectdomain<N> *arr=nd.is_rectangular_ex();
    UPCXXA_CHECKF((arr!=NULL),"not a rectdomain in cast.",
                  {UPCXXA_EMPTYREP(this); return;});
    rectdomain<N> rd=arr[0]; 
    p0 = rd.p0;
    p1 = rd.p1;
    loop_stride = rd.loop_stride;
  }
}
  
#ifdef UPCXXA_USE_CXX11
// use rvalue reference to avoid constructing a new temporary domain
template<int N> rectdomain<N>::rectdomain(domain<N> &&d) {
  if (d.is_empty()) {
    UPCXXA_EMPTYREP(this);
  } else {
    rectdomain<N> *arr=d.is_rectangular_ex();
    UPCXXA_CHECKF((arr!=NULL),"not a rectdomain in cast.",
                  {UPCXXA_EMPTYREP(this); return;});
    rectdomain<N> rd=arr[0]; 
    p0 = rd.p0;
    p1 = rd.p1;
    loop_stride = rd.loop_stride;
  }
}

# ifdef UPCXXA_USE_CXX11_INITIALIZER_LIST
template<int N> rectdomain<N>::rectdomain(const std::initializer_list< point<N> > &ilist) {
  auto iter = ilist.begin();
  bool stride_specified = false;
  p0 = *iter;
  ++iter;
  p1 = *iter;
  ++iter;
  if (iter == ilist.end()) {
    loop_stride = UPCXXA_ONES_POINT;
  } else {
    loop_stride = *iter;
    stride_specified = true;
  }
  if (!(p0 < p1))
    UPCXXA_EMPTYREP(this);
  else if (stride_specified) {
    UPCXXA_CHECK(loop_stride>=UPCXXA_ZERO_POINT,
                 "stride may not contain a negative point<N> value.");
    normalize();
  }
}
# endif
#endif

// Set Relations
template<int N> domain<N> rectdomain<N>::operator+(const rectdomain<N> &rd) const {
  // this + d

  // do union
  if (operator<=(rd)) {
    return rd.promote();
  } else if (operator>=(rd)) {
    return promote();
  } else {
    // Can assume this and d are not subsets of each other.  Note that this
    // assumption implies that neither are null sets.

    // information to create result domain
    cint_t expand_left = 0;
    cint_t expand_right = 0;
    cint_t set_stride = -1;
    bool has_change_dim = false;
    int change_dim;

    // "non encompassing" is where d does not encompass this
    // a "hand" of 0 is left, 1 is middle, 2 is right
    int num_nonencomp_dim = 0;
    int last_nonencomp_dim;
    int last_nonencomp_dim_hand;
    // non overlap is where d does not overlap this
    // a "hand" of 0 is left, 2 is right
    int num_nonoverlap_dim = 0;
    int last_nonoverlap_dim;
    int last_nonoverlap_dim_hand;
    // a dimension where d does not equal this
    int num_unequal_dim = 0;
    int last_unequal_dim;

    //Replacement for the removed stride parameters
    point<N> stride=get_stride();
    point<N> rdstride=rd.get_stride();

    // collect data
    for (int i = 0; i < N; i++) {
      if (p1.get0(i) <= rd.p0.get0(i)) {
        // cases:
        // +- this -+
        //            +- d -+
        num_nonoverlap_dim++;
        last_nonoverlap_dim = i;
        last_nonoverlap_dim_hand = 2;
        num_unequal_dim++;
        last_unequal_dim = i;
      } else if (p0.get0(i) >= rd.p1.get0(i)) {
        // cases:
        //         +- this -+
        // +- d -+
        num_nonoverlap_dim++;
        last_nonoverlap_dim = i;
        last_nonoverlap_dim_hand = 0;
        num_unequal_dim++;
        last_unequal_dim = i;
      } else if ((p0.get0(i) >= rd.p0.get0(i)) &&
                 (p1.get0(i) <= rd.p1.get0(i))) {
        // cases:
        //   +- this -+
        // +---- d -----+
        // - or -
        // +- this -+
        // +---- d ----+
        // - or -
        //    +- this -+
        // +---- d ----+
        // - or -
        // +- this -+
        // +-- d ---+
        if (!((p0.get0(i) == rd.p0.get0(i)) &&
              (p1.get0(i) == rd.p1.get0(i)))) {
          num_unequal_dim++;
          last_unequal_dim = i;
        }
      } else {
        num_unequal_dim++;
        last_unequal_dim = i;
        num_nonencomp_dim++;
        last_nonencomp_dim = i;
        if ((p0.get0(i) < rd.p0.get0(i)) && (p1.get0(i) > rd.p1.get0(i))) {
          // cases:
          // +-- this --+
          //   +- d -+
          last_nonencomp_dim_hand = 1;
        } else if ((p0.get0(i) < rd.p0.get0(i)) &&
                   (p1.get0(i) <= rd.p1.get0(i))) {
          // cases:
          // +- this -+
          //        +- d -+
          // - or -
          // +- this -+
          //    +- d -+
          last_nonencomp_dim_hand = 2;
        } else {
          // cases:
          //    +- this -+
          // +- d -+
          // - or -
          // +- this -+
          // +- d -+
          last_nonencomp_dim_hand = 0;
        }
      }
    }
      
    //this block is stride checked
    if ((num_nonoverlap_dim == 1) && (num_unequal_dim == 1)) {
      bool stridecompatible=true;
      for (int ar=0;ar<N && stridecompatible;ar++) {
        if (ar!=last_nonoverlap_dim)
          stridecompatible = stridecompatible && (stride.get0(ar)==rdstride.get0(ar));
      }
      if (stridecompatible) {
        if (stride.get0(last_nonoverlap_dim) == 0) {
          if (rdstride.get0(last_nonoverlap_dim) == 0) {
            if (rd.p0.get0(last_nonoverlap_dim) < p0.get0(last_nonoverlap_dim)) {
              // initiate stride left (stride regeneration)
              expand_left = (p0.get0(last_nonoverlap_dim) -
                             rd.p0.get0(last_nonoverlap_dim));
              set_stride = expand_left;
            } else {
              // initiate stride right (stride regeneration)
              expand_right = (rd.p0.get0(last_nonoverlap_dim) -
                              p0.get0(last_nonoverlap_dim));
              set_stride = expand_right;
            }
            has_change_dim = true;
            change_dim = last_nonoverlap_dim;
          } else {
	    if (p0.get0(last_nonoverlap_dim) ==
		(rd.p0.get0(last_nonoverlap_dim) -
		 rdstride.get0(last_nonoverlap_dim))) {
	      // right append to stride (stride regeneration)
	      expand_right = (rd.p1.get0(last_nonoverlap_dim) -
			      1 -
			      rd.p0.get0(last_nonoverlap_dim) +
			      rdstride.get0(last_nonoverlap_dim));
	      set_stride = rdstride.get0(last_nonoverlap_dim);
	      has_change_dim = true;
	      change_dim = last_nonoverlap_dim;
	    } else if (p0.get0(last_nonoverlap_dim) ==
		       (rd.p1.get0(last_nonoverlap_dim) +
			rdstride.get0(last_nonoverlap_dim) - 1)) {
	      // left append to stride (stride regeneration)
	      expand_left = (rd.p1.get0(last_nonoverlap_dim) -
			     1 -
			     rd.p0.get0(last_nonoverlap_dim) +
			     rdstride.get0(last_nonoverlap_dim));
	      set_stride = rdstride.get0(last_nonoverlap_dim);
	      has_change_dim = true;
	      change_dim = last_nonoverlap_dim;
	    }
	  }
	} else {
	  if ((rdstride.get0(last_nonoverlap_dim) ==
	       stride.get0(last_nonoverlap_dim)) ||
	      (rdstride.get0(last_nonoverlap_dim) == 0)) {
	    if (last_nonoverlap_dim_hand == 0) {
	      if ((rd.p1.get0(last_nonoverlap_dim) - 1) ==
		  (p0.get0(last_nonoverlap_dim) -
		   stride.get0(last_nonoverlap_dim))) {
		// abut left
		expand_left = (p0.get0(last_nonoverlap_dim) -
			       rd.p0.get0(last_nonoverlap_dim));
		has_change_dim = true;
		change_dim = last_nonoverlap_dim;
	      }
	    } else if (last_nonoverlap_dim_hand == 2) {
	      if (rd.p0.get0(last_nonoverlap_dim) ==
		  (p1.get0(last_nonoverlap_dim) -
		   1 +
		   stride.get0(last_nonoverlap_dim))) {
		// abut right
		expand_right = (rd.p1.get0(last_nonoverlap_dim) -
				p1.get0(last_nonoverlap_dim));
		has_change_dim = true;
		change_dim = last_nonoverlap_dim;
	      }
	    }
	  }
	}
      }
    }
    //this block is stride checked
    if ((!has_change_dim) &&
        (num_nonencomp_dim == 1) &&
        (num_unequal_dim == 1)) {
      if ((stride.get0(last_nonencomp_dim) != 0) &&
          (rdstride.get0(last_nonencomp_dim) ==
           stride.get0(last_nonencomp_dim))) {
        if (((rd.p0.get0(last_nonencomp_dim) -
              p0.get0(last_nonencomp_dim)) %
             stride.get0(last_nonencomp_dim)) == 0) {
          bool stridecompatible=true;
          for (int ar=0;ar<N;ar++) {
            if (ar!=last_nonencomp_dim)
              stridecompatible = stridecompatible && (stride.get0(ar)==rdstride.get0(ar));
          }
          if (stridecompatible) {
            if (last_nonencomp_dim_hand == 0) {
              // expand left equal stride
              expand_left = (p0.get0(last_nonencomp_dim) -
                             rd.p0.get0(last_nonencomp_dim));
              has_change_dim = true;
              change_dim = last_nonencomp_dim;
            } else if (last_nonencomp_dim_hand == 2) {
              // expand right equal stride
              expand_right = (rd.p1.get0(last_nonencomp_dim) -
                              p1.get0(last_nonencomp_dim));
              has_change_dim = true;
              change_dim = last_nonencomp_dim;
            }
          }
        }
      }
    }
    //this block is stride checked
    if ((!has_change_dim) &&
        (num_nonencomp_dim == 1) &&
        (num_unequal_dim == 1)) {
      if (stride.get0(last_nonencomp_dim) > 0) {
        if ((rdstride.get0(last_nonencomp_dim) == 0)
            ? false
            : (((stride.get0(last_nonencomp_dim) %
                 rdstride.get0(last_nonencomp_dim)) == 0) &&
               (rdstride.get0(last_nonencomp_dim) <
                stride.get0(last_nonencomp_dim)))) {
          bool stridecompatible=true;
          for (int ar=0;ar<N;ar++) {
            if (ar!=last_nonencomp_dim)
              stridecompatible = stridecompatible && (stride.get0(ar)==rdstride.get0(ar));
          }
          if (stridecompatible) {
            if (last_nonencomp_dim_hand == 0) {
              if ((rd.p0.get0(last_nonencomp_dim) <=
                   p0.get0(last_nonencomp_dim)) &&
                  (rd.p1.get0(last_nonencomp_dim) ==
		   (p1.get0(last_nonencomp_dim) -
		    rdstride.get0(last_nonencomp_dim)))) {
                // overlapped left interpolated points (stride reduction)
                expand_left = (p0.get0(last_nonencomp_dim) -
                               rd.p0.get0(last_nonencomp_dim));
                set_stride = rdstride.get0(last_nonencomp_dim);
                has_change_dim = true;
                change_dim = last_nonencomp_dim;
              }
            } else if (last_nonencomp_dim_hand == 2) {
              if ((rd.p1.get0(last_nonencomp_dim) >=
                   p1.get0(last_nonencomp_dim)) &&
                  (rd.p0.get0(last_nonencomp_dim) ==
                   (p0.get0(last_nonencomp_dim) +
                    rdstride.get0(last_nonencomp_dim)))) {
                // overlapped right interpolated points (stride reduction)
                expand_right = (rd.p1.get0(last_nonencomp_dim) -
                                p1.get0(last_nonencomp_dim));
                set_stride = rdstride.get0(last_nonencomp_dim);
                has_change_dim = true;
                change_dim = last_nonencomp_dim;
              }
            } else {
              if ((rd.p0.get0(last_nonencomp_dim) ==
                   (p0.get0(last_nonencomp_dim) +
                    rdstride.get0(last_nonencomp_dim))) &&
                  (rd.p1.get0(last_nonencomp_dim) ==
                   (p1.get0(last_nonencomp_dim) -
                    rdstride.get0(last_nonencomp_dim)))) {
                // overlapped middle interpolated points (stride reduction)
                set_stride = rdstride.get0(last_nonencomp_dim);
                has_change_dim = true;
                change_dim = last_nonencomp_dim;
              }
            }
          }
        }
      }
    }
    //this block is stride checked
    if ((!has_change_dim) &&
        (num_unequal_dim == 1)) {
      if (stride.get0(last_unequal_dim) > 0) {
        if ((stride.get0(last_unequal_dim) % 2) == 0) {
          cint_t check_stride = stride.get0(last_unequal_dim) / 2;
          if ((rdstride.get0(last_unequal_dim) ==
               stride.get0(last_unequal_dim)) ||
              (rdstride.get0(last_unequal_dim) == 0)) {
            bool stridecompatible=true;
            for (int ar=0;ar<N;ar++) {
              if (ar!=last_unequal_dim)
                stridecompatible = stridecompatible && (stride.get0(ar)==rdstride.get0(ar));
            }
            if (stridecompatible) {
              if ((rd.p0.get0(last_unequal_dim) ==
                   (p0.get0(last_unequal_dim) -
                    check_stride)) &&
                  (rd.p1.get0(last_unequal_dim) ==
                   (p1.get0(last_unequal_dim) -
                    check_stride))) {
                // non-overlapped left interpolated points (stride reduction)
                expand_left = check_stride;
                set_stride = check_stride;
                has_change_dim = true;
                change_dim = last_unequal_dim;
              } else if ((rd.p0.get0(last_unequal_dim) ==
                          (p0.get0(last_unequal_dim) +
                           check_stride)) &&
                         (rd.p1.get0(last_unequal_dim) ==
                          (p1.get0(last_unequal_dim) +
                           check_stride))) {
                // non-overlapped right interpolated points (stride reduction)
                expand_right = check_stride;
                set_stride = check_stride;
                has_change_dim = true;
                change_dim = last_unequal_dim;
              } else if ((rd.p0.get0(last_unequal_dim) ==
                          (p0.get0(last_unequal_dim) -
                           check_stride)) &&
                         (rd.p1.get0(last_unequal_dim) ==
                          (p1.get0(last_unequal_dim) +
                           check_stride))) {
                // non-overlapped left/right interpolated points (stride reduction)
                expand_left = check_stride;
                expand_right = check_stride;
                set_stride = check_stride;
                has_change_dim = true;
                change_dim = last_unequal_dim;
              } else if ((rd.p0.get0(last_unequal_dim) ==
                          (p0.get0(last_unequal_dim) +
                           check_stride)) &&
                         (rd.p1.get0(last_unequal_dim) ==
                          (p1.get0(last_unequal_dim) -
                           check_stride))) {
                // non-overlapped middle interpolated points (stride reduction)
                set_stride = check_stride;
                has_change_dim = true;
                change_dim = last_unequal_dim;
              }
            }
          }
        }
      }
    }
    //this block is stride checked
    if ((!has_change_dim) &&
        (num_unequal_dim == 1)) {
      if ((stride.get0(last_unequal_dim) != 0) &&
          (rdstride.get0(last_unequal_dim) != 0)) {
        if (((rdstride.get0(last_unequal_dim) %
              stride.get0(last_unequal_dim)) == 0) &&
            (rdstride.get0(last_unequal_dim) >
             stride.get0(last_unequal_dim))) {
          bool stridecompatible=true;
          for (int ar=0;ar<N;ar++) {
            if (ar!=last_unequal_dim)
              stridecompatible = stridecompatible && (stride.get0(ar)==rdstride.get0(ar));
          }
          if (stridecompatible) {
            //System.out.println("compatible");
	    if ((rd.p0.get0(last_unequal_dim) ==
		 (p0.get0(last_unequal_dim) - stride.get0(last_unequal_dim))) &&
		(rd.p1.get0(last_unequal_dim) <= p1.get0(last_unequal_dim))) {
	      // expand left greater stride
	      expand_left = stride.get0(last_unequal_dim);
	      has_change_dim = true;
	      change_dim = last_unequal_dim;
	    } else if ((rd.p0.get0(last_unequal_dim) >=
			p0.get0(last_unequal_dim)) &&
		       (rd.p1.get0(last_unequal_dim) ==
			(p1.get0(last_unequal_dim) +
			 stride.get0(last_unequal_dim)))) {
	      // expand right greater stride
	      expand_right = stride.get0(last_unequal_dim);
	      has_change_dim = true;
	      change_dim = last_unequal_dim;
	    } else if ((rd.p0.get0(last_unequal_dim) ==
			(p0.get0(last_unequal_dim) -
			 stride.get0(last_unequal_dim))) &&
		       (rd.p1.get0(last_unequal_dim) ==
			(p1.get0(last_unequal_dim) +
			 stride.get0(last_unequal_dim)))) {
	      // expand left/right greater stride
	      expand_left = stride.get0(last_unequal_dim);
	      expand_right = stride.get0(last_unequal_dim);
	      has_change_dim = true;
	      change_dim = last_unequal_dim;
	    }
	  }
        }
      }
    }
    //this block is stride checked
    if ((!has_change_dim) &&
        (num_unequal_dim == 1)) {
      bool stridecompatible=true;
      for (int ar=0;ar<N;ar++) {
        if (ar!=last_unequal_dim)
          stridecompatible = stridecompatible && (stride.get0(ar)==rdstride.get0(ar));
      }
      if (stridecompatible) {
        if ((stride.get0(last_unequal_dim) == 0) &&
            ((rdstride.get0(last_unequal_dim) % 2) == 0)) {
          cint_t check_stride = rdstride.get0(last_unequal_dim) / 2;
          if ((rd.p0.get0(last_unequal_dim) ==
               (p0.get0(last_unequal_dim) - check_stride)) &&
              (rd.p1.get0(last_unequal_dim) ==
               (p1.get0(last_unequal_dim) + check_stride))) {
            // initiate stride left/right (stride regeneration)
            expand_left = check_stride;
            expand_right = check_stride;
            set_stride = check_stride;
            has_change_dim = true;
            change_dim = last_unequal_dim;
          }
	    
        }
      }
    }

    // execute changes
    if (has_change_dim) {
      point<N> new_p0 = p0;
      point<N> new_p1 = p1;
      point<N> new_stride = stride;

      if (set_stride >= 0) {
        new_stride = new_stride.set0(change_dim, set_stride);
      }
      if (expand_left > 0) {
        new_p0 =
          new_p0.set0(change_dim,
                      (new_p0.get0(change_dim) -
                       domain_utils::align_up(expand_left,
                                              (new_stride.get0(change_dim) != 0)
                                              ? new_stride.get0(change_dim)
                                              : 1)));
      }
      if (expand_right > 0) {
        new_p1 = new_p1.set0(change_dim,
                             new_p1.get0(change_dim) + expand_right);
      }
      return (rectdomain<N>(new_p0, new_p1, new_stride)).promote();
    } else {
      domain<N> result = promote();
      result += rd;
      return result;
    }
  }
}
template<int N> domain<N> rectdomain<N>::operator-(const rectdomain<N> &rd) const {
  // this - d

  // do difference
  // information to create result domain
  cint_t shrink_left = 0;
  cint_t shrink_right = 0;
  cint_t mult_stride = 1;
  bool has_change_dim = false;
  int change_dim;

  rectdomain<N> di = operator*(rd);

  if (di.is_empty()) {
    // sets disjoint
    return promote();
  } else if (operator==(di)) {
    // null set
    return (rectdomain<N>()).promote();
  }

  // Replace the removed stride variable
  point<N> stride=get_stride();
  point<N> distride=di.get_stride();

  // "non encompassing" is where di does not encompass this
  // a "hand" of 0 is left, 1 is middle, 2 is right
  int num_nonencomp_dim = 0;
  int last_nonencomp_dim;
  int last_nonencomp_dim_hand;
  // when a dimension has the same limits, set if stride of di is 2x of this
  int num_equal_dim_stride_di_2x_this = 0;
  int last_equal_dim_stride_di_2x_this = 0;

  // collect data
  for (int i = 0; i < N; i++) {
    if ((p1.get0(i) <= di.p0.get0(i)) || (p0.get0(i) >= di.p1.get0(i))) {
      // cases:
      // +- this -+
      //            +- di -+
      // - or -
      //          +- this -+
      // +- di -+
    } else if ((p0.get0(i) >= di.p0.get0(i)) && (p1.get0(i) <= di.p1.get0(i))) {
      // cases:
      //   +- this -+
      // +---- di ----+
      // - or -
      // +- this -+
      // +---- di ----+
      // - or -
      //     +- this -+
      // +---- di ----+
      // - or -
      // +- this -+
      // +-- di --+
      if ((p0.get0(i) == di.p0.get0(i)) && (p1.get0(i) == di.p1.get0(i))) {
        if (stride.get0(i) != 0) {
          if (distride.get0(i) == (2*(stride.get0(i)))) {
            num_equal_dim_stride_di_2x_this++;
            last_equal_dim_stride_di_2x_this = i;
          }
        }
      }
    } else {
      num_nonencomp_dim++;
      last_nonencomp_dim = i;
      if ((p0.get0(i) < di.p0.get0(i)) && (p1.get0(i) > di.p1.get0(i))) {
        // cases:
        // +-- this --+
        //   +- di -+
        last_nonencomp_dim_hand = 1;
      } else if ((p0.get0(i) < di.p0.get0(i)) && (p1.get0(i) <= di.p1.get0(i))) {
        // cases:
        // +- this -+
        //        +- di -+
        // - or -
        // +- this -+
        //   +- di -+
        last_nonencomp_dim_hand = 2;
      } else {
        // cases:
        //    +- this -+
        // +- di -+
        // - or -
        // +- this -+
        // +- di -+
        last_nonencomp_dim_hand = 0;
      }
    }
  }

  // interpret collected data
  // this block is stride checked
  if (num_nonencomp_dim == 1) {
    if ((stride.set0(last_nonencomp_dim,distride.get0(last_nonencomp_dim)))==distride) { //stride compatibility check
      if ((last_nonencomp_dim_hand == 0) &&
          ((stride.get0(last_nonencomp_dim) ==
            distride.get0(last_nonencomp_dim)) ||
           distride.get0(last_nonencomp_dim) == 0)) {
        // slice off left side
        shrink_left = (di.p1.get0(last_nonencomp_dim) -
                       p0.get0(last_nonencomp_dim));
        has_change_dim = true;
        change_dim = last_nonencomp_dim;
      } else if ((last_nonencomp_dim_hand == 2) &&
                 ((stride.get0(last_nonencomp_dim) ==
                   distride.get0(last_nonencomp_dim)) ||
                  distride.get0(last_nonencomp_dim) == 0)) {
        // slice off right side
        shrink_right = (p1.get0(last_nonencomp_dim) -
                        di.p0.get0(last_nonencomp_dim));
        has_change_dim = true;
        change_dim = last_nonencomp_dim;
      } else if ((last_nonencomp_dim_hand == 1) &&
                 ((stride.get0(last_nonencomp_dim) == 0)
                  ? false
                  : (rectdomain<N>
                     (p0.set0(last_nonencomp_dim,
                              (p0.get0(last_nonencomp_dim) +
                               stride.get0(last_nonencomp_dim))),
                      p1.set0(last_nonencomp_dim,
                              (p1.get0(last_nonencomp_dim) -
                               stride.get0(last_nonencomp_dim))),
                      stride)) == (di))) {
        // middle elimination (stride magnification)
        mult_stride = ((p1.get0(last_nonencomp_dim) -
                        p0.get0(last_nonencomp_dim) - 1) /
                       stride.get0(last_nonencomp_dim));
        has_change_dim = true;
        change_dim = last_nonencomp_dim;
      }
    }
  }
  // the relevant subblock here is stride-checked
  if (!has_change_dim) {
    if (num_nonencomp_dim <= 1) {
      bool has_check_dim = false;
      if (num_nonencomp_dim == 1) {
        change_dim = last_nonencomp_dim;
        has_check_dim = true;
      } else {
        if (num_equal_dim_stride_di_2x_this == 1) {
          change_dim = last_equal_dim_stride_di_2x_this;
          has_check_dim = true;
        }
      }
      if (has_check_dim) {
        if ((stride.get0(change_dim) != 0) &&
            (distride.get0(change_dim) != 0)) {
          if ((stride.get0(change_dim)*2) == distride.get0(change_dim)) {
            if ((di.p1.get0(change_dim) == p1.get0(change_dim)) ||
                (di.p1.get0(change_dim) ==
                 (p1.get0(change_dim) - stride.get0(change_dim)))) {
              if ((stride.set0(change_dim,distride.get0(change_dim)))==distride) { //stride compatibility check
                if (p0.get0(change_dim) == di.p0.get0(change_dim)) {
                  // shifted interpolated points (stride magnification)
                  shrink_left = stride.get0(change_dim);
                  mult_stride = 2;
                  has_change_dim = true;
                } else if ((p0.get0(change_dim) + stride.get0(change_dim)) ==
                           di.p0.get0(change_dim)) {
                  // interpolated points (stride magnification)
                  mult_stride = 2;
                  has_change_dim = true;
                }
              }
            }
          }
        }
      }
    }
  }

  // execute changes
  if (has_change_dim) {
    point<N> new_p0 = p0;
    point<N> new_p1 = p1;
    point<N> new_stride = stride;

    if (shrink_left > 0) {
      new_p0 =
        new_p0.set0(change_dim,
                    (new_p0.get0(change_dim) +
                     domain_utils::align_up(shrink_left,
                                            (stride.get0(change_dim) != 0)
                                            ? stride.get0(change_dim)
                                            : 1)));
    }
    if (shrink_right > 0) {
      new_p1 = new_p1.set0(change_dim,
                           new_p1.get0(change_dim) - shrink_right);
    }
    if (mult_stride > 1) {
      new_stride = new_stride.set0(change_dim,
                                   (new_stride.get0(change_dim) *
                                    mult_stride));
    }
    return (rectdomain<N>(new_p0, new_p1, new_stride)).promote();
  } else {
    domain<N> result = promote();
    result -= rd;
    return result;
  }
}

template<int N> inline
rectdomain<N> rectdomain<N>::unit_stride_intersect(const rectdomain<N> &other) const {
  point<N> new_p0=p0,new_p1=p1;
  cint_t lbCor,ubCor; //lower and upper bounds
  for (int i=0;i<N;i++) {
    lbCor=domain_utils::max(p0.get0(i),other.p0.get0(i));
    ubCor=domain_utils::min(p1.get0(i),other.p1.get0(i));
    if (ubCor <= lbCor)
      //EMPTY SET
      return rectdomain<N>();
    new_p0=new_p0.set0(i,lbCor);
    new_p1=new_p1.set0(i,ubCor);
  }

  return rectdomain<N>(new_p0,new_p1,0.0f); //the new ctor
}

template<int N> inline rectdomain<N> &rectdomain<N>::operator*=(const rectdomain<N> &d) {
  *this = operator*(d);
  return *this;
}

template<int N> rectdomain<N> rectdomain<N>::operator*(const rectdomain<N> &rd)  const {
  // this * d
  // Intersection of a rectangle with a rectangle
    
  if ((loop_stride == UPCXXA_ONES_POINT) &&
      (rd.loop_stride == UPCXXA_ONES_POINT))
    return unit_stride_intersect(rd);
  point<N> new_p0;
  point<N> new_p1;
  point<N> new_stride;
  point<N> stride=get_stride();
  point<N> rdstride=rd.get_stride();
  if (p0 == rd.p0) {
    // p0 equal
    new_p0 = p0;
    new_p1 = p1.lower_bound(rd.p1);
    new_stride = stride.get_lcm(rdstride);
  } else {
    // p0 not equal
    bool detect_null_set = false;

    for (int i = 0; (i < N) && (!detect_null_set); i++) {
      cint_t start_p0 = p0.get0(i);
      cint_t start_d_p0 = rd.p0.get0(i);
      cint_t max_start = domain_utils::max(start_p0, start_d_p0);

      if (rdstride.get0(i) != 0) {
        start_d_p0 = (start_d_p0 +
                      domain_utils::align_up((max_start - start_d_p0),
                                             rdstride.get0(i)));
      }
      if (stride.get0(i) != 0) {
        start_p0 = (start_p0 +
                    domain_utils::align_up((domain_utils::max(max_start,
                                                              start_d_p0) -
                                            start_p0),
                                           stride.get0(i)));
      }

      if ((stride.get0(i) == 0) || (rdstride.get0(i) == 0)) {
        if (start_p0 != start_d_p0) {
          detect_null_set = true;
        } else {
          new_p0 = new_p0.set0(i, start_p0);
        }
      } else {
        bool found = false;
        for (cint_t k = 0; (k < stride.get0(i)) && (!found); k++) {
          if (((rdstride.get0(i) * k) % stride.get0(i)) ==
              ((start_p0 - start_d_p0) % stride.get0(i))) {
            found = true;
            new_p0 = new_p0.set0(i, start_d_p0 + k * rdstride.get0(i));
          }
        }
        if (!found) {
          detect_null_set = true;
        }
      }
    }

    if (detect_null_set) {
      // null set
      return rectdomain<N>();
    } else {
      new_p1 = p1.lower_bound(rd.p1);
      new_stride = stride.get_lcm(rdstride);
    }
  }

  return rectdomain<N>(new_p0, new_p1, new_stride);
}

template<int N> inline bool rectdomain<N>::is_empty() const {
  // this is empty
  // WARNING: the code-generator explicitly assumes this representation for empty RD
  // see code-call.cc:421
  return (loop_stride.get0(0) < 0);
}

template<int N> inline bool rectdomain<N>::is_not_empty() const {
  return !is_empty();
}

//is_adr() returns true iff the domain is a unit-stride rectangle or union of same
//      (ADR stands for "all dense rects")
// Since singular rectangles are still unit stride, we can use loop_stride
template<int N> inline bool rectdomain<N>::is_adr() const {
  return is_empty() || loop_stride == UPCXXA_ONES_POINT;
}

template<int N> inline bool rectdomain<N>::operator==(const rectdomain<N> &rd) const {
  return equals(rd);
}

template<int N> inline bool rectdomain<N>::equals(const rectdomain<N> &rd) const {
  // this == d
  return ((is_empty() && rd.is_empty()) || (p0 == rd.p0 &&
                                          p1 == rd.p1 &&
                                          loop_stride == rd.loop_stride));
}

template<int N> inline bool rectdomain<N>::operator!=(const rectdomain<N> &rd) const {
  return !(operator==(rd));
}

template<int N> bool rectdomain<N>::operator<=(const rectdomain<N> &rd) const { // isSubset
  // this <= d
    
  //The empty set is a subset of any set
  if (is_empty()) return true;
    
  //The subset must be contained within the bounding box of rd
  if (!(p0 >= rd.p0) || !(p1 <= rd.p1)) return false;
    
  //If we are within the bounding box, and are unit stride, then we are a subset
  if ((loop_stride == UPCXXA_ONES_POINT) &&
      (rd.loop_stride == UPCXXA_ONES_POINT)) return true;
    
  //If our strides are offset such that the bottom corners don't line up, we are not a subset
  if (!rd.contains(p0)) return false;
    
  for (int i=0; i < N ; i++) {
    cint_t stride_i =
      (UPCXXA_STRIDE_I_ZERO(this,i)) ? 0 : loop_stride.get0(i);
    if ( UPCXXA_STRIDE_I_ZERO(&rd,i)) {
      if ( stride_i != 0 )
        //If the other set is singular in dimension i and we are not, we must not be a subset
        return false;
    } else if ( (stride_i % rd.loop_stride.get0(i)) != 0 )
      //If our stride is not divisible by the other's stride, we are not a subset
      return false;
  }
  return true;
}

template<int N> inline bool rectdomain<N>::operator<(const rectdomain<N> &rd) const { // isStrictSubset
  // this < d
  return (operator<=(rd)) && !(operator==(rd));
}

template<int N> inline bool rectdomain<N>::operator>=(const rectdomain<N> &rd) const { // isSuperset
  return rd <= *this;
}

template<int N> inline bool rectdomain<N>::operator>(const rectdomain<N> &rd) const { // isStrictSuperset
  return rd < *this;
}

// Domain relations
template<int N> inline domain<N> rectdomain<N>::operator+(const domain<N> &d) const {
  domain<N> result = promote();
  result += d;
  return result;
}

template<int N> inline domain<N> rectdomain<N>::operator*(const domain<N> &d) const {
  domain<N> result = promote();
  result *= d;
  return result;
}

template<int N> inline domain<N> rectdomain<N>::operator-(const domain<N> &d) const {
  domain<N> result = promote();
  result -= d;
  return result;
}

template<int N> inline bool rectdomain<N>::operator==(const domain<N> &d) const {
  return equals(d);
}

template<int N> inline bool rectdomain<N>::equals(const domain<N> &d) const {
  return promote().equals(d);
}

template<int N> inline bool rectdomain<N>::operator!=(const domain<N> &d) const {
  return !(operator==(d));
}

template<int N> inline bool rectdomain<N>::operator<=(const domain<N> &d) const { // isSubset
  return promote() <= d;
}

template<int N> inline bool rectdomain<N>::operator<(const domain<N> &d) const { // isStrictSubset
  return promote() < d;
}

template<int N> inline bool rectdomain<N>::operator>=(const domain<N> &d) const { // isSuperset
  return promote() >= d;
}

template<int N> inline bool rectdomain<N>::operator>(const domain<N> &d) const { // isStrictSuperset
  return promote() > d;
}

// rectdomain Resizing
template<int N> rectdomain<N> rectdomain<N>::resize(cint_t k, int dir, cint_t s) const {
  if (is_empty()) return *this;
  int direction;
  point<N> new_p0, new_p1, new_stride;

  UPCXXA_CHECK(s > 0,"s <= 0 in accrete/shrink.");
  UPCXXA_CHECK(abs(dir) > 0 && abs(dir) <= N,
               "bad dir in accrete/shrink.");

  new_p0 = p0;
  new_p1 = p1;
  new_stride = loop_stride;
  direction = domain_utils::direction_value(dir);

  if ((s == loop_stride.get0(direction)) ||
      (UPCXXA_STRIDE_I_ZERO(this,direction)) ||
      ((k+1)*s >= loop_stride.get0(direction) &&
       loop_stride.get0(direction) % s == 0)) { // PR668
    if (domain_utils::direction_backward(dir)) {
      new_p0 = p0.set0(direction, p0.get0(direction) - k*s);
    } else {
      new_p1 = p1.set0(direction, p1.get0(direction) + k*s);
    }
    new_stride = loop_stride.set0(direction, s);

    return rectdomain<N>(new_p0, new_p1, new_stride);
  } else {
    // !!! error - result not a Rect domain
    std::cout << "Runtime: (Error) not a rectdomain in accrete/shrink." << std::endl;
    return rectdomain<N>();
  }
}

template<int N> inline rectdomain<N> rectdomain<N>::accrete(cint_t k, int dir, cint_t s) const {
  UPCXXA_CHECK(k >= 0,"k < 0 in accrete.");
  return resize(k, dir, s);
}

template<int N> inline rectdomain<N> rectdomain<N>::accrete(cint_t k, int dir) const {
  UPCXXA_CHECK(k >= 0,"k < 0 in accrete.");
  return resize(k, dir, 1);
}

template<int N> inline rectdomain<N> rectdomain<N>::accrete(cint_t k, const point<N> &p) const {
  UPCXXA_CHECK(k >= 0,"k < 0 in accrete.");
  rectdomain<N> result = *this;
  for (int i = 1; i <= N; i++) {
    result = result.resize(k, i, p.get0(domain_utils::direction_value(i)));
    result = result.resize(k, -i, p.get0(domain_utils::direction_value(-i)));
  }
  return result;
}

template<int N> inline rectdomain<N> rectdomain<N>::accrete(cint_t k) const {
  UPCXXA_CHECK(k >= 0,"k < 0 in accrete.");
  return accrete(k, UPCXXA_ONES_POINT);
}

template<int N> inline rectdomain<N> rectdomain<N>::shrink(cint_t k, int dir) const {
  int dir_v=domain_utils::direction_value(dir);
  return resize(-k,
                dir,
                loop_stride.get0(dir_v));
}

template<int N> inline rectdomain<N> rectdomain<N>::shrink(cint_t k) const {
  rectdomain<N> result = *this;
  for (int i = 1; i <= N; i++) {
    result = result.resize(-k,
                           i,
                           loop_stride.get0(domain_utils::direction_value(i)));
    result = result.resize(-k,
                           -i,
                           loop_stride.get0(domain_utils::direction_value(-i)));
  }
  return result;
}

template<int N> rectdomain<N> rectdomain<N>::border(cint_t k, int dir, cint_t s) const {
  if (is_empty()) return *this;
  int direction;
  point<N> new_p0, new_p1;

  new_p0 = p0;
  new_p1 = p1;
  direction = domain_utils::direction_value(dir);
  UPCXXA_CHECK((k >= loop_stride.get0(direction)),
               "border requires k >= stride()[|dir|]");
  if (domain_utils::direction_backward(dir)) {
    new_p1 = new_p1.set0(direction, new_p0.get0(direction) - s + k);
    new_p0 = new_p0.set0(direction, new_p0.get0(direction) - s);
  } else {
    new_p0 = new_p0.set0(direction, new_p1.get0(direction) + s - k);
    new_p1 = new_p1.set0(direction, new_p1.get0(direction) + s);
  }

  return rectdomain<N>(new_p0, new_p1, loop_stride.set0(direction, 1), 0.0f);
}

template<int N> inline rectdomain<N> rectdomain<N>::border(cint_t k, int dir) const {
  return border(k, dir, 1);
}

template<int N> inline rectdomain<N> rectdomain<N>::border(int dir) const {
  return border(1, dir, 1);
}

// Point Relations
template<int N> inline rectdomain<N> rectdomain<N>::operator+(const point<N> &p) const {
  // this + p
  //translation will not affect validity
  assert((rectdomain<N>(p0+p,p1+p,loop_stride))==(rectdomain<N>(p0+p,p1+p,loop_stride,0.0f))); //,"op+(p)");
  return rectdomain<N>(p0 + p, p1 + p, loop_stride,0.0f);
}

template<int N> inline rectdomain<N> rectdomain<N>::operator-(const point<N> &p) const {
  // this - p
  //translation will not affect validity
  assert((rectdomain<N>(p0-p,p1-p,loop_stride))==(rectdomain<N>(p0-p,p1-p,loop_stride,0.0f)));
  return rectdomain<N>(p0 - p, p1 - p, loop_stride,0.0f);
}

template<int N> rectdomain<N> rectdomain<N>::operator*(const point<N> &p) const {
  // this * p
  if (is_empty()) return *this;

  point<N> new_p0 = p0 * p;
  point<N> new_p1 = (p1 - UPCXXA_ONES_POINT) * p;
  point<N> new_stride = get_stride() * p;

  for (int i = 0; i < N; i++) {
    if (p.get0(i) < 0) {
      cint_t saved_p0 = new_p0.get0(i);
      new_p0 = new_p0.set0(i, new_p1.get0(i));
      new_p1 = new_p1.set0(i, saved_p0);
      new_stride = new_stride.set0(i, -new_stride.get0(i));
    }
    //Bench to see if this is faster than just calling the normalizing ctor
    if (new_stride.get0(i) == 0)
      new_stride = new_stride.set0(i,1);
  }
  new_p1 = new_p1 + UPCXXA_ONES_POINT;

  return rectdomain<N>(new_p0, new_p1, new_stride,0.0f);
}

template<int N> rectdomain<N> rectdomain<N>::operator/(const point<N> &p) const {
  // this / p
  if (is_empty()) return *this;

  // TEST THIS
  point<N> new_p0 = p0 / p;
  point<N> new_p1 = (p1 - UPCXXA_ONES_POINT)/p;
  point<N> new_stride = loop_stride / p;

  for (int i = 0; i < N; i++) {
    if (loop_stride.get0(i) < p.get0(i)) {
      new_stride = new_stride.set0(i, 1);
    }
    if (p.get0(i) < 0) {
      cint_t saved_p0 = new_p0.get0(i);
      new_p0 = new_p0.set0(i, new_p1.get0(i));
      new_p1 = new_p1.set0(i, saved_p0);
      new_stride = new_stride.set0(i, -new_stride.get0(i));
    }
  }
  new_p1 = new_p1 + UPCXXA_ONES_POINT;
  //Not confident in proper behavior here to use the new ctor
  return rectdomain<N>(new_p0, new_p1, new_stride);
}
/* op-UPCXXA_ASSIGN operators */

template<int N> inline rectdomain<N> &rectdomain<N>::operator+=(const point<N> &p) {
  *this = operator+(p);
  return *this;
}

template<int N> inline rectdomain<N> &rectdomain<N>::operator-=(const point<N> &p) {
  *this = operator-(p);
  return *this;
}

template<int N> inline rectdomain<N> &rectdomain<N>::operator*=(const point<N> &p) {
  *this = operator*(p);
  return *this;
}

template<int N> inline rectdomain<N> &rectdomain<N>::operator/=(const point<N> &p) {
  *this = operator/(p);
  return *this;
}

template<int N> inline rectdomain<N> rectdomain<N>::permute(const point<N> &p) const {
  //TEST THIS - DO WE NEED GETSTRIDE FOR THE CTOR?
  //I don't think permutation affects validity
  UPCXXA_CHECK_PERMUTE(p, *this);
  assert((rectdomain<N>(p0.permute(p),p1.permute(p),loop_stride.permute(p)))==(rectdomain<N>(p0.permute(p),p1.permute(p),loop_stride.permute(p),0.0f)));
  return rectdomain<N>(p0.permute(p),
                       p1.permute(p),
                       loop_stride.permute(p),0.0f);
}

template<int N> inline rectdomain<N> rectdomain<N>::translate(const point<N> &p) const {
  // this + p
  assert((rectdomain<N>(p0+p,p1+p,loop_stride))==(rectdomain<N>(p0+p,p1+p,loop_stride,0.0f)));
  return rectdomain<N>(p0 + p, p1 + p, loop_stride,0.0f);
}

// Shape information
template<int N> bool rectdomain<N>::contains(const point<N> &p) const {
  // this contains p
  if ((p >= p0) && (p < p1)) {
    for (int i = 0; i < N; i++) {
      cint_t st = loop_stride.get0(i);
      if (UPCXXA_STRIDE_I_ZERO(this,i)) {
        if (p.get0(i) != p0.get0(i))
          return false;
      } else if (st != 1) { //skip st==1
        if (((p.get0(i) - p0.get0(i)) % st) != 0) {
          return false;
        }
      }
    }
    return true;
  }
  return false;
}

template<int N> inline bool rectdomain<N>::is_rectangular() const {
  return true;
}

template<int N> inline size_t rectdomain<N>::size() const {
  if (is_empty()) return 0;
  size_t side_mult = 1;
  for (int i = 0; i < N; i++) {
    side_mult *= (size_t)(((p1.get0(i) - p0.get0(i) - 1)/loop_stride.get0(i))+1);
  }
  return side_mult;
}

template<int N> inline point<N> rectdomain<N>::min() const {
  return p0;
}

template<int N> inline point<N> rectdomain<N>::lwb() const {
  return p0;
}

template<int N> inline point<N> rectdomain<N>::upb() const {
  return p1;
}

template<int N> inline point<N> rectdomain<N>::max() const {
  return p1 - (UPCXXA_ONES_POINT);
}

template<int N> inline point<N> rectdomain<N>::get_stride() const {
  if (is_empty()) return UPCXXA_ZERO_POINT;
  point<N> retval=loop_stride;
  for (int i=0;i<N;i++)
    if (UPCXXA_STRIDE_I_ZERO(this,i))
      retval=retval.set0(i,0);

  return retval;
}

template<int N> inline point<N> rectdomain<N>::stride() const {
  UPCXXA_CHECK(is_not_empty(),
               "called stride() on an empty rectdomain");
  return loop_stride;
}
  
template<int N> inline point<N> rectdomain<N>::raw_stride() const {
  return loop_stride;
}
  
template<int N> inline rectdomain<N> rectdomain<N>::bounding_box() const {
  if (is_empty()) return *this; // The special ctor doesn't catch empty and set stride correctly
  return rectdomain<N>(min(),upb(),0.0f);
}

template<int N> int rectdomain<N>::get_num_side_points(int dir) const {
  if (UPCXXA_STRIDE_I_ZERO(this,dir)) {
    if (p1.get0(dir) <= p0.get0(dir)) {
      return 0;
    } else {
      return 1;
    }
  } else {
    return ((p1.get0(dir) - 1 - p0.get0(dir)) / loop_stride.get0(dir)) + 1;
  }
}

template<int N> inline domain<N> rectdomain<N>::promote() const {
  // Promote the object to be represented in a more general type.
  // For now, always promote to a Mulrectdomain<N>A.
  return domain<N>(*this);
}

template<int N> rectdomain<UPCXXA_SLICE_ARITY> rectdomain<N>::slice(int k) const {
#ifdef UPCXXA_USE_CXX11
  static_assert(N > 1, "slice undefined for N <= 1");
#else
  if (N <= 1) {
    std::cerr << "slice undefined for N <= 1 (thread = "
              << UPCXXA_DMYPROC << ")" << std::endl;
    abort();
  }
#endif
  return slice_internal(k);
}

template<int N>
rectdomain<UPCXXA_SLICE_ARITY> rectdomain<N>::slice_internal(int k) const {
  k--; // make it zero-based
  if (loop_stride == UPCXXA_ONES_POINT) {
    point<UPCXXA_SLICE_ARITY> new_p0, new_p1;
    for (int i=0, j=0; i < N; i++) {
      if (i != k) {
        new_p0 = new_p0.set0(j, p0.get0(i));
        new_p1 = new_p1.set0(j, p1.get0(i));
        j++;
      }
    }
    //If the dimensions were fine before, they'll be fine now too
    assert(((rectdomain<UPCXXA_SLICE_ARITY>(new_p0,new_p1))==
            (rectdomain<UPCXXA_SLICE_ARITY>(new_p0,new_p1,true))));
    return rectdomain<UPCXXA_SLICE_ARITY>(new_p0, new_p1,true);
  } else {
    point<UPCXXA_SLICE_ARITY> new_p0, new_p1, new_stride;
    //point<N> stride=get_stride();
    for (int i=0, j=0; i < N; i++) {
      if (i != k) {
        new_p0 = new_p0.set0(j, p0.get0(i));
        new_p1 = new_p1.set0(j, p1.get0(i));
        new_stride = new_stride.set0(j, loop_stride.get0(i));
        j++;
      }
    }
    //If the dimensions were valid before, they'll be fine now too
    assert(((rectdomain<UPCXXA_SLICE_ARITY>(new_p0,new_p1,new_stride))==
            (rectdomain<UPCXXA_SLICE_ARITY>(new_p0,new_p1,new_stride,true))));
    return rectdomain<UPCXXA_SLICE_ARITY>(new_p0, new_p1, new_stride,true);
  }
}

template<int N> inline size_t rectdomain<N>::index(const point<N> &p) const {
  int i;
  cint_t sum = 0;
  cint_t sideFactor = 1;

  for (i = N; i >= 1; i--) {
    /* At this point, x->stride[i-1] should divide qm.  Of course, we only
       know for sure if bounds checking is on. */
    const cint_t str = raw_stride()[i];
    const cint_t q = p[i];
    const cint_t qm = q - min()[i];
    cint_t delta = qm * sideFactor;
    if (N > 1)
      sideFactor *= (1 + UPCXXA_PFAST_DIVIDE((upb()[i] - 1 -
                                              min()[i]),
                                             raw_stride()[i]));

    if (str != 1) delta /= str; /* only do the expensive divide when necessary */
    sum += delta;
  }
  return (size_t) sum;
}

} // namespace upcxx
